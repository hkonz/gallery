//----------------------------------------------
// sequelize
const Sequelize = require("sequelize");
const sequelize = new Sequelize(
  process.env.DB_NAME,
  process.env.DB_USER,
  process.env.DB_PASS,
  {
    host: process.env.DB_HOST,
    dialect: process.env.DB_DIALECT,
    dialectOptions: {
      charset: "utf8mb4"
    }
  }
);
// ---------------
// bootstrap
const bootstrap = require("./bootstrap");

// ---------------
// log connection
sequelize
  .authenticate()
  .then(function() {
    console.log("Connection has been established successfully.");
  })
  .catch(function(err) {
    console.error("Unable to connect to the database:", err);
  });

// ---------------
// connect all models/tables to one object,
// so everything is accessible once db is required/imported
const db = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;

// ---------------
// models/tables

db.User = require("../models/user.model")(sequelize, Sequelize);
db.File = require("../models/file.model")(sequelize, Sequelize);
db.Item = require("../models/item.model")(sequelize, Sequelize);
db.Tag = require("../models/tag.model")(sequelize, Sequelize);
db.Role = require("../models/role.model")(sequelize, Sequelize);

// ---------------
// relations

// file
db.FilesItem = db.File.belongsTo(db.Item, {
  onDelete: "CASCADE",
  hooks: true
});

// item
db.ItemUser = db.Item.belongsTo(db.User, { as: "owner" });
db.ItemFiles = db.Item.hasMany(db.File, { as: "files" });
db.ItemCategory = db.Item.belongsTo(db.Tag, { as: "category" });
db.ItemTag = db.Item.belongsToMany(db.Tag, {
  through: "item_tag",
  as: "tags",
  foreignKey: "itemId"
});

// tag
db.TagRole = db.Tag.belongsTo(db.Role, { as: "role" });
db.CategoryItem = db.Tag.hasMany(db.Item, {
  as: "categoryItems",
  foreignKey: "categoryId"
});
db.TagItem = db.Tag.belongsToMany(db.Item, {
  through: "item_tag",
  as: "items",
  foreignKey: "tagId"
});

// user
db.UserRole = db.User.belongsToMany(db.Role, {
  through: "user_role",
  as: "roles",
  foreignKey: "userId"
});

// role
db.RoleUser = db.Role.belongsToMany(db.User, {
  through: "user_role",
  as: "users",
  foreignKey: "roleId"
});

db.RoleTag = db.Role.hasOne(db.Tag);

// ---------------
// sync

sequelize.sync({ alter: true }).then(() => {
  bootstrap(db);
});

module.exports = db;
