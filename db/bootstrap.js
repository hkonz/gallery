const roles = require("../client/src/constants/roles");

const role_keys = Object.keys(roles);

const role_map = role_keys.map(key => {
  return { name: roles[key], type: "FEATURE" };
});

module.exports = async db => {
  try {
    const { Op } = db.Sequelize;

    // db.Tag.create(
    //   {
    //     name: "Ohne Kategorie",
    //     code: "cat-misc",
    //     isCategory: true,
    //     sequence: 999,
    //     role: {
    //       name: "ROLE_KATEGORIE_OHNE",
    //       type: "CATEGORY"
    //     }
    //   },
    //   {
    //     include: [db.TagRole]
    //   }
    // );

    const guestRoles = await db.Role.findAll({
      where: {
        type: ["CATEGORY", "TAG"],
        name: { [Op.ne]: "ROLE_PRIVAT" }
      }
    });

    // users
    const [admin] = await db.User.findOrCreate({
      where: {
        username: "11"
      },
      defaults: {
        password: "12345"
      }
    });

    const [guest] = await db.User.findOrCreate({
      where: {
        username: "gast"
      },
      defaults: {
        password: "gast"
      }
    });

    let adminRole = await db.Role.findOne({
      where: {
        name: roles.ADMIN
      }
    });

    if (!adminRole) {
      // feature roles
      await db.Role.bulkCreate(role_map).then(async () => {
        adminRole = await db.Role.findOne({
          where: {
            name: roles.ADMIN
          }
        });
      });
    }
    guest.setRoles(guestRoles);
    admin.setRoles([adminRole]);
  } catch (err) {
    console.log(err.message);
  }
};
