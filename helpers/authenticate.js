const jwt = require("jsonwebtoken");
const db = require("../db/database");
const { User, Role } = db;
const ADMIN_ROLE = "ROLE_ADMIN";

async function findAndSetUser(req, res, next) {
  try {
    const token = req.headers.authorization.split(" ")[1];
    const payload = await jwt.verify(token, process.env.JWT_SECRET);

    if (payload) {
      const user = await User.findByPk(payload.id);
      req.user = user;
      next();
    } else {
      next();
    }
  } catch (e) {
    next();
  }
}

async function checkUser(req, res, next) {
  // if user exists the token was sent with the request
  if (req.user) {
    //if user exists then go to next middleware
    next();
  }
  // token was not sent with request send error to user
  else {
    res.status(401).json({ error: "login is required" });
  }
}

async function findAndSetRoles(req, res, next) {
  try {
    req.userRoles = await req.user.getRoles();

    next();
  } catch (err) {
    res.status(400).json({ error: err.msg });
  }
}

function checkRoles(givenRoles = []) {
  let authorizedRoles = [];

  if (typeof givenRoles === "string") {
    authorizedRoles = givenRoles.split(",");
  } else {
    authorizedRoles = givenRoles;
  }

  return async function(req, res, next) {
    if (!authorizedRoles.length) {
      // no role specified, so go to next
      next();
    } else {
      let userRoles = req.userRoles.map(role => role.get({ plain: true }).name);

      if (userRoles.includes(ADMIN_ROLE)) {
        // user is admin, so
        next();
      } else {
        const authorized = authorizedRoles.every(role =>
          userRoles.includes(role)
        );
        if (authorized) {
          next();
        } else {
          res.status(403).json({ error: "not authorized" });
        }
      }
    }
  };
}

function auth(roles) {
  return [findAndSetUser, checkUser, findAndSetRoles, checkRoles(roles)];
}

module.exports = auth;
