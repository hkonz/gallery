const groupBy = require("lodash/groupBy");
const DELIMITER = "_||_";

const monthNames = [
  "Januar",
  "Februar",
  "März",
  "April",
  "Mai",
  "Juni",
  "Juli",
  "August",
  "September",
  "Oktober",
  "November",
  "Dezember"
];

const createArrayFromGroups = (groupedItems, sortFn) => {
  const result = [];
  const groupNames = Object.keys(groupedItems);

  groupNames.sort(sortFn).forEach(groupName => {
    const groupObject = {
      name: groupName.split(DELIMITER)[1],
      items: groupedItems[groupName]
    };
    result.push(groupObject);
  });
  return result;
};

const categories = items => {
  const groupedByCategories = groupBy(items, item => {
    const { category } = item;

    if (category) {
      return category.sequence + DELIMITER + category.name;
    } else {
      return "Z" + DELIMITER + "Ohne Kategorie";
    }
  });

  return createArrayFromGroups(groupedByCategories);
};

const years = items => {
  const groupedItems = groupBy(items, item => {
    const { createdAt, eventDate } = item;
    const dateToCheck = eventDate ? eventDate : createdAt;

    const year = dateToCheck.getFullYear();
    return "" + year + DELIMITER + year;
  });

  return createArrayFromGroups(groupedItems, (a, b) => a - b);
};

const months = items => {
  const groupedItems = groupBy(items, item => {
    const { createdAt, eventDate } = item;
    const dateToCheck = eventDate ? eventDate : createdAt;

    const month = dateToCheck.getMonth();
    const year = dateToCheck.getFullYear();
    return "" + year + addZ(month) + DELIMITER + monthNames[month] + " " + year;
  });

  return createArrayFromGroups(groupedItems, (a, b) => a - b);
};

const days = items => {
  const groupedItems = groupBy(items, item => {
    const { createdAt, eventDate } = item;
    const dateToCheck = eventDate ? eventDate : createdAt;

    const day = dateToCheck.getDate();
    const month = dateToCheck.getMonth();
    const year = dateToCheck.getFullYear();
    return (
      "" +
      year +
      addZ(month) +
      addZ(day) +
      DELIMITER +
      day +
      ". " +
      monthNames[month] +
      " " +
      year
    );
  });

  return createArrayFromGroups(groupedItems, (a, b) => a - b);
};

function addZ(n) {
  return n < 10 ? "0" + n : "" + n;
}

module.exports = {
  categories,
  years,
  months,
  days
};
