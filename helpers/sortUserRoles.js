// todo: refactor constants
const ADMIN_ROLE = "ROLE_ADMIN";
const ROLE_TYPE_TAG = "TAG";
const ROLE_TYPE_CATEGORY = "CATEGORY";

module.exports = function sortUserRoles(userRoles = []) {
  const categoryRoles = [];
  const tagRoles = [];
  let isAdmin = false;
  userRoles.forEach(seq_role => {
    const role = seq_role.get({ plain: true });
    if (role.name === ADMIN_ROLE) {
      isAdmin = true;
    } else if (role.type === ROLE_TYPE_CATEGORY) {
      categoryRoles.push(role.id);
    } else if (role.type === ROLE_TYPE_TAG) {
      tagRoles.push(role.id);
    }
  });
  return {
    isAdmin,
    tagRoles,
    categoryRoles
  };
};
