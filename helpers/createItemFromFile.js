const sharp = require("sharp");
const db = require("../db/database");

const THUMB_SIZE = {
  width: 180,
  height: 180
};

const fs = require("fs");
const uuidv4 = require("uuid/v4");
const path = require("path");
const ffmpeg = require("fluent-ffmpeg");

const ffmpeg_path = path.resolve(__dirname, "../ffmpeg/bin/ffmpeg.exe");
const ffprobe_path = path.resolve(__dirname, "../ffmpeg/bin/ffprobe.exe");
ffmpeg.setFfmpegPath(ffmpeg_path);
ffmpeg.setFfprobePath(ffprobe_path);

const createItemFromFile = async req => {
  const { category, eventDate, tags, id } = req.body;
  const tagIds = tags ? tags.split(",") : [];
  const { buffer, mimetype, size, originalname } = req.file;
  const [type, extension] = mimetype.split("/");
  const { File, Item, Tag } = db;

  if (type === "image") {
    // get File options
    const files = [];
    const originalValues = await processImageBuffer(buffer);
    originalValues.type = "original";
    const thumbSize = getResizeOptionsForThumb(originalValues);
    const thumbValues = await processImageBuffer(buffer, thumbSize);
    thumbValues.type = "thumb";

    files.push(originalValues, thumbValues);

    let mediumValues;

    const mediumSize = getResizeOptionsForMedium(originalValues);
    if (mediumSize) {
      mediumValues = await processImageBuffer(buffer, mediumSize);
      mediumValues.type = "medium";
      files.push(mediumValues);
    }

    // create File records
    // const original = await File.create(originalValues);
    // const thumb = await File.create(thumbValues);
    // const medium = mediumValues ? await File.create(mediumValues) : null;

    const item = await Item.create(
      {
        id,
        filename: originalname,
        categoryId: category,
        eventDate: eventDate,
        files,
        type
      },
      {
        include: [db.ItemFiles]
      }
    );
    // set Tags
    if (tagIds.length) await item.setTags(tagIds);
    // set Owner
    await item.setOwner(req.user);
  } else if (type === "video") {
    const folder = path.resolve(__dirname, "tmp");
    const input_filename = uuidv4() + "." + extension;
    const tmp_filename = uuidv4() + ".png";
    const input_fd = folder + "\\" + input_filename;
    const tmp_fd = folder + "\\" + tmp_filename;

    const input_file = await fs.writeFile(input_fd, buffer, function() {
      console.log("successfully created temp input file: " + input_fd);
      ffmpeg(input_fd)
        .screenshots({
          timestamps: ["51%"],
          filename: tmp_filename,
          folder: folder
        })
        .on("error", function(err) {
          throw new Error(err);
        })
        .on("end", async function() {
          console.log("successfully created temp screenshot: " + tmp_fd);

          // 1. get values for original record
          const videoDimensions = await getSizeFromImage(tmp_fd);
          const originalValues = {
            size,
            buffer,
            mimetype,
            width: videoDimensions.width,
            height: videoDimensions.height,
            type: "original"
          };
          // 2. get values for thumb record
          const thumbSize = getResizeOptionsForThumb(originalValues);
          const thumbValues = await processImageBuffer(tmp_fd, thumbSize);
          thumbValues.type = "thumb";

          // 3. get values for screenshot record
          const screenshotValues = await processImageBuffer(tmp_fd, {});
          screenshotValues.type = "screenshot";

          // todo: create medium vids
          const item = await Item.create(
            {
              id,
              type,
              filename: originalname,
              files: [originalValues, thumbValues, screenshotValues],
              categoryId: category,
              eventDate: eventDate
            },
            {
              include: [db.ItemFiles]
            }
          );

          // 4. set Tags
          if (tagIds.length) await item.setTags(tagIds);

          // 5. delete temp files
          fs.unlink(tmp_fd, function() {
            console.log("successfully deleted temp screenshot file: ", tmp_fd);
          });

          fs.unlink(input_fd, function() {
            console.log("successfully deleted temp input file: ", input_fd);
          });
        });
    });
  } else {
    console.log("unprocessable file uploaded");
  }

  return await Item.findByPk(id, {
    include: [
      {
        model: File,
        as: "files",
        attributes: { exclude: ["buffer"] }
      },
      {
        model: Tag,
        as: "category"
      },
      {
        model: Tag,
        as: "tags"
      }
    ]
  });
};

const processImageBuffer = async (inputBuffer, resizeOptions) => {
  let buffer;
  if (!resizeOptions) {
    buffer = inputBuffer;
  } else {
    buffer = await sharp(inputBuffer)
      .rotate()
      .resize(resizeOptions)
      .jpeg()
      .toBuffer();
  }
  const meta = await sharp(buffer).metadata();

  return {
    buffer,
    mimetype: "image/" + meta.format,
    size: meta.size,
    width: meta.width,
    height: meta.height
  };
};

const getResizeOptionsForMedium = fileOptions => {
  const { width, height } = fileOptions;
  const max_a = 1024;
  const max_b = 1920;
  const MEDIUM_WIDTH = 1024;

  const isBigger =
    (width > max_a && height > max_b) || (width > max_b && height > max_a);

  const longSide = width >= height ? "width" : "height";

  if (isBigger) {
    return {
      [longSide]: MEDIUM_WIDTH
    };
  }
  return null;
};

const getResizeOptionsForThumb = fileOptions => {
  const { width, height } = fileOptions;

  const shortSide = width <= height ? "width" : "height";

  return {
    [shortSide]: THUMB_SIZE[shortSide]
  };
};

const getSizeFromImage = async input => {
  const meta = await sharp(input).metadata();
  return {
    width: meta.width,
    height: meta.height
  };
};

const getResizeOptions = options => {
  const { gridWidth, gridHeight } = options;
  const { width, height } = thumbSize;

  return {
    width: gridWidth * width,
    height: gridHeight * height
  };
};

const getSizeTypeFromResizeOptions = resizeOptions => {
  let result = "medium";
};

module.exports = {
  createItemFromFile
};
