const bcrypt = require("bcrypt");
const saltRounds = 10;

module.exports = (sequelize, Sequelize) => {
  const User = sequelize.define(
    "user",
    {
      // attributes
      username: {
        type: Sequelize.STRING,
        allowNull: false,
        unique: "user_name"
      },
      password: {
        type: Sequelize.STRING,
        allowNull: false
      },
      firstName: {
        type: Sequelize.STRING
      },
      lastName: {
        type: Sequelize.STRING
        // allowNull defaults to true
      }
    },
    {
      // options
      hooks: {
        beforeCreate: async function(user) {
          const salt = await bcrypt.genSalt(saltRounds);
          user.password = await bcrypt.hash(user.password, salt);
        }
      }
    }
  );
  User.prototype.verifyPassword = async function(password) {
    return await bcrypt.compare(password, this.password);
  };

  return User;
};
