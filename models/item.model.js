const File = require("./file.model");
const uuid = require("uuid/v4");
const {
  processImageBuffer,
  getResizeOptions
} = require("../services/file.service");

module.exports = (sequelize, Sequelize) => {
  // make sure db/client/connection can support emoji
  sequelize.query("SET NAMES utf8mb4;");
  const Model = sequelize.define(
    "item",
    {
      // attributes
      id: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.UUID,
        defaultValue: uuid()
      },
      gridWidth: {
        type: Sequelize.INTEGER,
        allowNull: false,
        defaultValue: 1
      },
      gridHeight: {
        type: Sequelize.INTEGER,
        allowNull: false,
        defaultValue: 1
      },
      filename: {
        type: Sequelize.STRING(255),
        allowNull: false,
        defaultValue: ""
      },
      comment: {
        type: Sequelize.STRING(512),
        allowNull: true,
        defaultValue: ""
      },
      eventDate: {
        type: Sequelize.DATE
      },
      type: {
        type: Sequelize.STRING(128),
        allowNull: false,
        defaultValue: ""
      }
    },
    {
      // options
      charset: "utf8mb4",
      hooks: {
        async afterUpdate(instance, options) {
          const hasGridWidthChanged = instance.changed("gridWidth");
          const hasGridHeightChanged = instance.changed("gridHeight");

          if (!hasGridWidthChanged && !hasGridHeightChanged) {
            // grid sizes don't changed, so...
            return;
          }

          const files = await instance.getFiles();

          const gridSize = {
            gridWidth: instance.getDataValue("gridWidth"),
            gridHeight: instance.getDataValue("gridHeight")
          };

          const resizeOptions = getResizeOptions(gridSize);

          if (files.length) {
            const thumb = files.find(
              file => file.getDataValue("type") === "thumb"
            );
            const original = files.find(
              file => file.getDataValue("type") === "original"
            );
            const screenshot = files.find(
              file => file.getDataValue("type") === "screenshot"
            );

            const processableFile =
              original.getDataValue("mimetype").split("/")[0] === "image"
                ? original
                : screenshot;

            const updatedThumbProps = await processImageBuffer(
              processableFile.getDataValue("buffer"),
              resizeOptions
            );
            const updatedThumb = await thumb.update(updatedThumbProps);
          }
        }
      }
    }
  );

  return Model;
};
