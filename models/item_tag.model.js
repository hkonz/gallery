module.exports = (sequelize, Sequelize) => {
  return sequelize.define(
    "item_tag",
    {
      // attributes
      itemId: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: {
          model: "item",
          key: "id"
        }
      },
      tagId: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: {
          model: "tag",
          key: "id"
        }
      }
    },
    {
      // options
    }
  );
};
