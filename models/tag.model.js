module.exports = (sequelize, Sequelize) => {
  return sequelize.define(
    "tag",
    {
      // attributes
      name: {
        type: Sequelize.STRING,
        allowNull: false,
        unique: "uniqueTagsAndCategories"
      },
      code: {
        type: Sequelize.STRING,
        allowNull: false,
        unique: "tag_code"
      },
      isCategory: {
        type: Sequelize.BOOLEAN,
        allowNull: false,
        defaultValue: false,
        unique: "uniqueTagsAndCategories"
      },
      sequence: {
        type: Sequelize.INTEGER,
        defaultValue: 1
      }
    },
    {
      // options
      charset: "utf8mb4"
    }
  );
};
