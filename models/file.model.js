module.exports = (sequelize, Sequelize) => {
  return sequelize.define(
    "file",
    {
      // attributes
      width: {
        type: Sequelize.INTEGER,
        allowNull: false,
        defaultValue: 0
      },
      height: {
        type: Sequelize.INTEGER,
        allowNull: false,
        defaultValue: 0
      },
      size: {
        type: Sequelize.BIGINT,
        allowNull: false,
        defaultValue: 0
      },
      type: {
        type: Sequelize.ENUM("original", "thumb", "medium", "screenshot"),
        allowNull: false,
        defaultValue: "original"
      },
      buffer: {
        type: Sequelize.BLOB("long"),
        allowNull: true
      },
      mimetype: {
        type: Sequelize.STRING(80),
        allowNull: false,
        defaultValue: ""
      }
    },
    {
      // options
      charset: "utf8mb4",
      scopes: {
        withoutBuffer: {
          attributes: { exclude: ["buffer"] }
        }
      }
    }
  );
};
