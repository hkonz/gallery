module.exports = (sequelize, Sequelize) => {
  return sequelize.define(
    "user_role",
    {
      // attributes
      userId: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: {
          model: "user",
          key: "id"
        }
      },
      roleId: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: {
          model: "role",
          key: "id"
        }
      }
    },
    {
      // options
    }
  );
};
