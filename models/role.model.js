module.exports = (sequelize, Sequelize) => {
  return sequelize.define(
    "role",
    {
      // attributes
      name: {
        type: Sequelize.STRING,
        allowNull: false,
        unique: "role_name"
      },
      type: {
        type: Sequelize.ENUM("TAG", "FEATURE", "CATEGORY"),
        allowNull: false,
        defaultValue: "TAG"
      },
      description: {
        type: Sequelize.STRING(255)
      }
    },
    {
      // options
    }
  );
};
