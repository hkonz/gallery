import React from "react";
import AppBar from "../components/UI/AppBar";
import UploadForm from "../components/Upload/Form";
import Container from "@material-ui/core/Container";

function ScreenUpload(props) {
  return (
    <div>
      <AppBar />
      <Container>
        <UploadForm />
      </Container>
    </div>
  );
}

export default ScreenUpload;
