import React from "react";

import Grid from "../components/Grid/Container";
import AppBar from "../components/UI/AppBar";
import BottomBar from "../components/UI/BottomBar";

function Gallery(props) {
  return (
    <React.Fragment>
      <AppBar />
      <Grid />
      <BottomBar />
    </React.Fragment>
  );
}

export default Gallery;
