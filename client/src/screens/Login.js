import React from "react";

import { makeStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";

import Login from "../components/Login";

const useStyles = makeStyles(theme => ({
  root: {
    margin: theme.spacing(3, 5),
    padding: theme.spacing(3, 4),
    maxWidth: "580px"
  }
}));

export default function PaperSheet() {
  const classes = useStyles();

  return (
    <div>
      <Paper className={classes.root}>
        <Login />
      </Paper>
    </div>
  );
}
