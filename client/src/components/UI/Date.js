import React from "react";

const monthNames = [
  "Januar",
  "Februar",
  "März",
  "April",
  "Mai",
  "Juni",
  "Juli",
  "August",
  "September",
  "Oktober",
  "November",
  "Dezember"
];

export default function DateString(props) {
  const { date } = props;
  const _date = new Date(date);
  const day = _date.getDate();
  const month = _date.getMonth();
  const year = _date.getFullYear();

  return <span>{day + ". " + monthNames[month] + " " + year}</span>;
}
