import React from "react";
import Checkbox from "@material-ui/core/Checkbox/index";
import FormGroup from "@material-ui/core/FormGroup/index";
import FormControlLabel from "@material-ui/core/FormControlLabel/index";
import FormControl from "@material-ui/core/FormControl/index";

import { makeStyles } from "@material-ui/core/styles";
import { connect } from "react-redux";
import { addTypeFilter, removeTypeFilter } from "../../actions/group.actions";
import SmallLabel from "./SmallLabel";

const useStyles = makeStyles(theme => {
  return {
    formControlLabel: {
      fontSize: "0.875rem",
      fontWeight: theme.typography.fontWeightRegular
    },
    checkbox_root: {
      color: theme.palette.grey[300]
    }
  };
});

function CategoryFilter(props) {
  const { addFilter, removeFilter } = props;
  const classes = useStyles();

  function handleChange(event) {
    const { checked, value } = event.target;

    checked ? addFilter(value) : removeFilter(value);
  }

  return (
    <div>
      <SmallLabel>Filter:</SmallLabel>
      <FormControl component="fieldset">
        <FormGroup
          aria-label="position"
          name="position"
          onChange={handleChange}
          row
        >
          <FormControlLabel
            value="image"
            control={
              <Checkbox
                color="primary"
                classes={{ root: classes.checkbox_root }}
              />
            }
            label="Bilder"
            labelPlacement="end"
            classes={{ label: classes.formControlLabel }}
          />
          <FormControlLabel
            value="video"
            control={
              <Checkbox
                color="primary"
                classes={{ root: classes.checkbox_root }}
              />
            }
            label="Videos"
            labelPlacement="end"
            classes={{ label: classes.formControlLabel }}
          />
        </FormGroup>
      </FormControl>
    </div>
  );
}

const mapDispatch = {
  addFilter: addTypeFilter,
  removeFilter: removeTypeFilter
};

export default connect(
  null,
  mapDispatch
)(CategoryFilter);
