import React from "react";
import { connect } from "react-redux";
import { makeStyles } from "@material-ui/core/styles";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import { selectGroup } from "../../actions/group.actions";
import { getGroup, getGroups } from "../../selectors";
import SmallLabel from "./SmallLabel";

const useStyles = makeStyles(theme => {
  return {
    tabs_root: {
      minHeight: theme.spacing(5.25)
    },
    tabs_indicator: {
      height: "4px"
    },
    tab_root: {
      textTransform: "inherit",
      fontSize: "0.875rem",
      fontWeight: theme.typography.fontWeightRegular,
      minWidth: "inherit",
      minHeight: theme.spacing(5.25)
    },
    tab_textColorPrimary: {
      color: theme.palette.text.primary
    }
  };
});

function SortByTabs(props) {
  const { groups, selectGroup, selectedGroup } = props;
  const classes = useStyles();

  const buttons = groups.map(group => {
    return (
      <Tab
        key={group.name}
        value={group.name}
        label={group.label}
        classes={{
          root: classes.tab_root,
          textColorPrimary: classes.tab_textColorPrimary
        }}
      />
    );
  });

  return (
    <div>
      <SmallLabel>Sortierung</SmallLabel>
      <Tabs
        value={selectedGroup}
        indicatorColor="primary"
        textColor="primary"
        classes={{ root: classes.tabs_root, indicator: classes.tabs_indicator }}
        onChange={(e, val) => selectGroup(val)}
      >
        {buttons}
      </Tabs>
    </div>
  );
}

const mapState = state => {
  return {
    selectedGroup: getGroup(state),
    groups: getGroups(state)
  };
};

const mapDispatch = {
  selectGroup
};

export default connect(
  mapState,
  mapDispatch
)(SortByTabs);
