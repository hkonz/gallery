import React, { useState } from "react";
import { connect } from "react-redux";
import { makeStyles } from "@material-ui/core/styles";
import SideBar from "./SideBar";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Button from "@material-ui/core/Button";
import Container from "@material-ui/core/Container";
import IconButton from "@material-ui/core/IconButton";
import MenuIcon from "@material-ui/icons/Menu";
import { logout } from "../../actions/auth.actions";
import { changeScreen, toggleSelectionMode } from "../../actions/ui.actions";
import IfAllGranted from "../UI/IfAllGranted";
import roles from "../../constants/roles";
import Icon from "@mdi/react";
import { mdiTarget } from "@mdi/js";

const useStyles = makeStyles(theme => ({
  headerBar: {
    background: `linear-gradient(45deg, ${theme.palette.primary.main} 30%, ${
      theme.palette.primary.light
    } 90%)`,
    height: "4px"
  },
  wrapper: {
    flexGrow: 1,
    position: "relative",
    zIndex: 1
  },
  appBarRoot: {
    boxShadow: theme.shadows[2]
  },
  menuButton: {
    marginRight: theme.spacing(1)
  },
  title: {
    flexGrow: 1,
    fontWeight: 700,
    fontSize: "1.20rem"
  },
  gutters: {
    paddingLeft: 0,
    paddingRight: 0
  },
  regular: {
    minHeight: "56px"
  }
}));

function AppNavBar(props) {
  const { logout, changeScreen, toggleSelectionMode } = props;
  const [isSideBarOpen, setIsSideBarOpen] = useState(false);
  const classes = useStyles();

  function toggleSideBar() {
    setIsSideBarOpen(!isSideBarOpen);
  }
  return (
    <div className={classes.wrapper}>
      <div className={classes.headerBar} />
      <SideBar open={isSideBarOpen} toggleSideBar={toggleSideBar} />
      <AppBar
        position="static"
        color="inherit"
        classes={{ root: classes.appBarRoot }}
      >
        <Container>
          <Toolbar
            classes={{ gutters: classes.gutters, regular: classes.regular }}
          >
            <IconButton
              edge="start"
              className={classes.menuButton}
              color="inherit"
              aria-label="Menu"
              onClick={toggleSideBar}
            >
              <MenuIcon />
            </IconButton>
            <span variant="h6" className={classes.title}>
              album.konz.net
            </span>
            <IfAllGranted roles={roles.ITEM_UPDATE}>
              <IconButton
                edge="start"
                className={classes.menuButton}
                color="inherit"
                aria-label="Menu"
                onClick={() => toggleSelectionMode()}
              >
                <Icon path={mdiTarget} size={"18px"} />
              </IconButton>
            </IfAllGranted>

            <Button color="inherit" onClick={logout}>
              Abmelden
            </Button>
          </Toolbar>
        </Container>
      </AppBar>
    </div>
  );
}

const mapDispatch = {
  logout,
  changeScreen,
  toggleSelectionMode
};

export default connect(
  null,
  mapDispatch
)(AppNavBar);
