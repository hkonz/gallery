import React from "react";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles(theme => {
  return {
    label: {
      ...theme.typography.overline,
      color: theme.palette.text.hint,
      fontWeight: theme.typography.fontWeightBold,
      borderBottom: "1px solid " + theme.palette.grey[300]
    }
  };
});

export default function SmallLabel(props) {
  const classes = useStyles();
  return null;
  //   return <div className={classes.label}>{props.children}</div>;
}
