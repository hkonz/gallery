import React from "react";
import SortbyTabs from "./SortByTabs";
import TypeFilter from "./TypeFilter";
import { makeStyles } from "@material-ui/core/styles";

const useStyle = makeStyles(theme => {
  return {
    wrapper: {
      justifyContent: "space-between",
      backgroundColor: "white",
      flexWrap: "wrap",
      display: "flex"
    }
  };
});

function ControlsBar(props) {
  const classes = useStyle();
  return (
    <div className={classes.wrapper}>
      <TypeFilter />
      <SortbyTabs />
    </div>
  );
}

export default ControlsBar;
