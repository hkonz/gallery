import { connect } from "react-redux";
import { getAuthUser } from "../../selectors";

import role from "../../constants/roles";

function IfAllGranted(props) {
  const { user, roles, children } = props;

  let roleToCheck = typeof roles === "string" ? roles.split(",") : roles;

  if (user) {
    const rolesGranted = user.roles || [];
    if (!rolesGranted.length) {
      return null;
    } else if (rolesGranted.includes(role.ADMIN)) {
      return children;
    } else if (roleToCheck.every(role => rolesGranted.includes(role))) {
      return children;
    } else {
      return null;
    }
  }
}

const mapState = state => {
  return {
    user: getAuthUser(state)
  };
};

export default connect(mapState)(IfAllGranted);
