import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import SwipeableDrawer from "@material-ui/core/SwipeableDrawer";
import SideBarNav from "./SideBarNav";

function SideBar(props) {
  const { open, toggleSideBar } = props;
  return (
    <SwipeableDrawer
      anchor="left"
      open={open}
      onClose={toggleSideBar}
      onOpen={toggleSideBar}
    >
      <SideBarNav />
    </SwipeableDrawer>
  );
}

export default SideBar;
