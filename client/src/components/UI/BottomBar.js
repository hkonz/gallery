import React, { useEffect } from "react";
import { connect } from "react-redux";
import { makeStyles } from "@material-ui/core/styles";
import CategoryList from "../CategoryList/CategoryList";
import TagSelect from "../LightBox/TagSelect";
import DateSelect from "../LightBox/DateSelect";
import AppBar from "@material-ui/core/AppBar";
import Divider from "@material-ui/core/Divider";
import Toolbar from "@material-ui/core/Toolbar";
import IconButton from "@material-ui/core/IconButton";
import {
  getIsSelectionModeActive,
  getSelectedItems,
  getCategories,
  getTags
} from "../../selectors";
import { deleteItems, putBulkItemUpdate } from "../../actions/item.actions";
import IfAllGranted from "../UI/IfAllGranted";
import roles from "../../constants/roles";
import Icon from "@mdi/react";
import {
  mdiTrashCanOutline,
  mdiClose,
  mdiContentSave,
  mdiCancel
} from "@mdi/js";
import { getCategoryList } from "../../actions/category.actions";
import { getTagList } from "../../actions/tag.actions";
import {
  clearSelectedItems,
  toggleSelectionMode
} from "../../actions/ui.actions";

const useStyles = makeStyles(theme => ({
  text: {
    padding: theme.spacing(2, 2, 0)
  },
  paper: {
    paddingBottom: 50
  },
  list: {
    marginBottom: theme.spacing(2)
  },
  subheader: {
    backgroundColor: theme.palette.background.paper
  },
  appBar: {
    top: "auto",
    bottom: 0
  },
  grow: {
    flexGrow: 1
  },
  fabButton: {
    position: "absolute",
    zIndex: 1,
    top: -30,
    left: 0,
    right: 0,
    margin: "0 auto"
  },
  divider: {
    width: 1,
    height: 28,
    margin: 4
  },
  toolbar: {
    padding: theme.spacing(1, 2),
    justifyContent: "space-between"
  },
  selectionPanel: {
    backgroundColor: "white",
    color: "black",
    padding: theme.spacing(1, 2)
  },
  selectionPanelSection: {
    padding: theme.spacing(1, 1)
  }
}));

const initalUpdateValues = { data: { tags: [] } };

function BottomBar(props) {
  const {
    isSelectionModeActive,
    selectedItems,
    deleteItems,
    updateItems,
    categories,
    getCategoryList,
    getTagList,
    clearSelectedItems,
    tags,
    toggleSelectionMode
  } = props;
  const [updates, setUpdates] = React.useState(initalUpdateValues);
  const classes = useStyles();

  useEffect(() => {
    getCategoryList();
    getTagList();
  }, [getCategoryList, getTagList]);

  function handleDelete() {
    deleteItems(selectedItems);
  }

  function handleChange(options) {
    setUpdates({
      data: {
        ...updates.data,
        ...options
      }
    });
  }

  function handleUpdate() {
    const data = selectedItems.map(id => ({
      id,
      ...updates.data
    }));
    updateItems(data);
    setUpdates(initalUpdateValues);
    clearSelectedItems();
  }

  function clearForm() {
    setUpdates(initalUpdateValues);
  }

  console.log("updates", updates);

  return (
    <>
      {isSelectionModeActive && (
        <AppBar position="fixed" color="primary" className={classes.appBar}>
          <Toolbar classes={{ root: classes.toolbar }}>
            <span style={{ display: "flex" }}>
              <IfAllGranted roles={roles.ITEM_UPDATE}>
                <IconButton color="inherit" onClick={toggleSelectionMode}>
                  <Icon path={mdiClose} size={"18px"} color="white" />
                </IconButton>
                <Divider className={classes.divider} />
                <IconButton color="inherit" onClick={clearForm}>
                  <Icon path={mdiCancel} size={"18px"} color="white" />
                </IconButton>
              </IfAllGranted>
            </span>
            <span style={{ display: "flex" }}>
              <IfAllGranted roles={roles.ITEM_UPDATE}>
                <IconButton color="inherit" onClick={handleUpdate}>
                  <Icon path={mdiContentSave} size={"18px"} color="white" />
                </IconButton>
              </IfAllGranted>
              <IfAllGranted roles={roles.ITEM_DELETE}>
                <Divider className={classes.divider} />
                <IconButton color="inherit" onClick={handleDelete}>
                  <Icon path={mdiTrashCanOutline} size={"18px"} color="white" />
                </IconButton>
              </IfAllGranted>
            </span>
          </Toolbar>
          <div className={classes.selectionPanel}>
            <div className={classes.selectionPanelSection}>
              <CategoryList
                categories={categories}
                onSelect={categoryId => handleChange({ categoryId })}
                iconSize={"18px"}
                iconColor={"black"}
              />
              {updates.data.categoryId && (
                <span>{categories[updates.data.categoryId].name}</span>
              )}
            </div>
            <div className={classes.selectionPanelSection}>
              <DateSelect
                onChange={moment =>
                  handleChange({ eventDate: moment.toDate() })
                }
                value={updates.data.eventDate || null}
              />
            </div>
            <div className={classes.selectionPanelSection}>
              <TagSelect
                onChange={event => handleChange({ tags: event.target.value })}
                availableTags={tags}
                value={updates.data.tags || []}
              />
            </div>
          </div>
        </AppBar>
      )}
    </>
  );
}

const mapState = state => {
  return {
    isSelectionModeActive: getIsSelectionModeActive(state),
    selectedItems: getSelectedItems(state),
    categories: getCategories(state),
    tags: getTags(state)
  };
};

const mapDispatch = {
  deleteItems: deleteItems,
  updateItems: putBulkItemUpdate,
  getCategoryList: getCategoryList,
  getTagList: getTagList,
  clearSelectedItems: clearSelectedItems,
  toggleSelectionMode: toggleSelectionMode
};

export default connect(
  mapState,
  mapDispatch
)(BottomBar);
