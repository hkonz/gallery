import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import { connect } from "react-redux";
import List from "@material-ui/core/List";
import Divider from "@material-ui/core/Divider";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import { logout } from "../../actions/auth.actions";
import { changeScreen } from "../../actions/ui.actions";
import { getActiveScreen } from "../../selectors";
import IfAllGranted from "../UI/IfAllGranted";
import roles from "../../constants/roles";

const useStyles = makeStyles({
  list: {
    width: 250
  },
  fullList: {
    width: "auto"
  }
});

function SideBarNav(props) {
  const { activeScreen, changeScreen } = props;
  const classes = useStyles();
  return (
    <div className={classes.list} role="presentation">
      <List>
        <IfAllGranted roles={roles.UPLOAD}>
          <ListItem
            button
            onClick={() => changeScreen("upload")}
            selected={activeScreen === "upload"}
          >
            <ListItemText primary={"Upload"} />
          </ListItem>
        </IfAllGranted>
        <ListItem
          button
          onClick={() => changeScreen("gallery")}
          selected={activeScreen === "gallery"}
        >
          <ListItemText primary={"Gallery"} />
        </ListItem>
      </List>
      <Divider />
      <List>
        <ListItem
          button
          disabled
          onClick={() => changeScreen("user")}
          selected={activeScreen === "user"}
        >
          <ListItemText primary={"Users"} />
        </ListItem>
      </List>
    </div>
  );
}

const mapState = state => {
  return {
    activeScreen: getActiveScreen(state)
  };
};

const mapDispatch = {
  logout,
  changeScreen
};

export default connect(
  mapState,
  mapDispatch
)(SideBarNav);
