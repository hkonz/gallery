import React, { useState, useEffect } from "react";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import FormGroup from "@material-ui/core/FormGroup";
import Typography from "@material-ui/core/Typography";

import { connect } from "react-redux";
import { authSuccess, login, logout } from "../../actions/auth.actions";
import { getAuth } from "../../selectors";

function Login(props) {
  const { fetching, isAuthenticated, logout, login, authSuccess } = props;

  const [creds, setCreds] = useState({
    username: "",
    password: ""
  });
  const { username, password } = creds;

  // = componentDidMount()
  useEffect(() => {
    const userJson = localStorage.getItem("user");
    if (userJson) {
      const user = JSON.parse(userJson);
      authSuccess({ user });
    }
  }, [authSuccess]);

  function handleChange(e) {
    const { name, value } = e.target;
    setCreds({
      ...creds,
      [name]: value
    });
  }

  return (
    <form
      onSubmit={e => {
        e.preventDefault();
        login(creds);
      }}
    >
      {isAuthenticated ? (
        <Button variant="contained" color="primary" onClick={logout}>
          Logout
        </Button>
      ) : (
        <React.Fragment>
          <Typography variant="h6">Anmelden</Typography>

          <FormGroup>
            <TextField
              name="username"
              label="Benutzername"
              value={username}
              onChange={handleChange}
              type="text"
              margin="normal"
            />
          </FormGroup>
          <FormGroup>
            <TextField
              name="password"
              label="Passwort"
              value={password}
              onChange={handleChange}
              type="password"
              margin="normal"
            />
          </FormGroup>
          <Button
            variant="contained"
            color="primary"
            type={"submit"}
            disabled={fetching}
          >
            Login
          </Button>
        </React.Fragment>
      )}
    </form>
  );
}

const mapState = state => {
  return {
    ...getAuth(state)
  };
};

const mapDispatch = {
  login: login,
  logout: logout,
  authSuccess: authSuccess
};

export default connect(
  mapState,
  mapDispatch
)(Login);
