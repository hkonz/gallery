import { useReducer } from "react";
import _omit from "lodash/omit";

export default function useUploadReducer() {
  const initialUploads = {};
  const uploadReducer = (state, action) => {
    switch (action.type) {
      case "UPLOAD_ADD":
        return {
          ...state,
          [action.payload.id]: action.payload
        };
      case "UPLOAD_UPDATE":
        return {
          ...state,
          [action.payload.id]: {
            ...state[action.payload.id],
            ...action.payload
          }
        };
      case "UPLOAD_REMOVE":
        return _omit(state, action.payload);
      default:
        return state;
    }
  };

  const [uploads, dispatch] = useReducer(uploadReducer, initialUploads);

  function addUpload(uploadOptions) {
    dispatch({ type: "UPLOAD_ADD", payload: uploadOptions });
  }

  function removeUpload(id) {
    dispatch({ type: "UPLOAD_REMOVE", payload: id });
  }

  function updateProgress(id, progress) {
    dispatch({ type: "UPLOAD_UPDATE", payload: { id, progress } });
  }

  function updateStatus(id, state) {
    dispatch({ type: "UPLOAD_UPDATE", payload: { id, state } });
  }

  function updateUploadProp(id, props) {
    dispatch({ type: "UPLOAD_UPDATE", payload: { id, ...props } });
  }

  return {
    uploads,
    addUpload,
    removeUpload,
    updateProgress,
    updateStatus,
    updateUploadProp
  };
}
