import React, { useState } from "react";

import { makeStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";

const useStyles = makeStyles(theme => {
  return {
    input: {
      display: "none"
    }
  };
});

function UploadButton(props) {
  const { onChange } = props;
  const classes = useStyles();
  return (
    <div>
      <input
        className={classes.input}
        id="contained-button-file"
        multiple
        type="file"
        onChange={onChange}
      />
      <label htmlFor="contained-button-file">
        <Button variant="outlined" component="span" className={classes.button}>
          Durchsuchen...
        </Button>
      </label>
    </div>
  );
}

export default UploadButton;
