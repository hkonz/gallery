import React from "react";
import { useTheme } from "@material-ui/core/styles";
import Icon from "@mdi/react";
import { mdiCheckBold, mdiSettings, mdiAlertOutline } from "@mdi/js";

function StateIcon({ state }) {
  const theme = useTheme();
  return (
    <>
      {state === "completed" && (
        <Icon
          path={mdiCheckBold}
          size={"18px"}
          color={theme.palette.secondary.main}
        />
      )}
      {state === "failed" && (
        <Icon path={mdiAlertOutline} size={"18px"} color={"red"} />
      )}
      {state === "processing" && (
        <Icon
          path={mdiSettings}
          size={"18px"}
          color={theme.palette.grey[300]}
          spin
        />
      )}
    </>
  );
}

export default StateIcon;
