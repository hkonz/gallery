import React, { useState, useReducer, useEffect } from "react";
import { connect } from "react-redux";
import {
  getCategoryList,
  createCategory
} from "../../actions/category.actions";
import { getTagList, createTag } from "../../actions/tag.actions";
import UploadButton from "./UploadButton";
import UploadList from "./List";
import Button from "@material-ui/core/Button";
import { getTags, getCategories } from "../../selectors";
import uuid from "uuidv4";
import axios from "axios";
import { API_ROOT } from "../../config/config";
import getAuthHeader from "../../helpers/getAuthHeaders";
import { startUploads, addUpload } from "../../actions/upload.action";
import useUploadReducer from "./reducer.hook";
import { normalizeItem } from "../../helpers/normalize";
import { addEntities } from "../../actions/entities.actions";
import CategoryList from "../CategoryList/CategoryList";
import TagSelect from "../LightBox/TagSelect";
import DateSelect from "../LightBox/DateSelect";
import { Paper } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import CreateTag from "../UI/CreateTag";

const useStyles = makeStyles(theme => {
  return {
    spacer: {
      marginTop: theme.spacing(2)
    }
  };
});

function UploadForm(props) {
  const {
    getCategoryList,
    getTagList,
    allCategories,
    allTags,
    addEntities,
    createCategory,
    createTag
  } = props;

  const {
    uploads,
    addUpload,
    removeUpload,
    updateProgress,
    updateStatus,
    updateUploadProp
  } = useUploadReducer();

  const [globals, setGlobals] = useState({
    category: Object.keys(allCategories)[0],
    tags: [],
    eventDate: new Date()
  });

  const classes = useStyles();

  function handleFileSelect(event) {
    const files = event.target.files;
    for (let key in files) {
      if (files.hasOwnProperty(key)) {
        const file = files[key];
        const id = uuid();
        const state = "init";
        const newUpload = {
          id,
          file,
          state,
          progress: 0
        };

        addUpload(newUpload);
        //
      }
    }
  }

  function handleUpload() {
    for (let id in uploads) {
      if (uploads.hasOwnProperty(id) && uploads[id].state !== "completed") {
        (async function sendRequest() {
          const data = {
            ...globals,
            ...uploads[id]
          };
          try {
            updateStatus(id, "uploading");
            const response = await axios.post(
              API_ROOT + "/upload",
              getFormDatafromObject(data),
              {
                headers: {
                  "Content-Type": "multipart/form-data",
                  ...getAuthHeader().headers
                },
                onUploadProgress: progressEvent => {
                  let percentCompleted = Math.floor(
                    (progressEvent.loaded * 100) / progressEvent.total
                  );
                  updateProgress(id, percentCompleted);
                  if (percentCompleted === 100) {
                    updateStatus(id, "processing");
                  }
                }
              }
            );
            const { entities } = normalizeItem(response.data);
            console.log(entities);

            addEntities(entities);
            updateStatus(id, "completed");
          } catch (err) {
            updateStatus(id, "failed");
          }
        })();
      }
    }
  }

  useEffect(() => {
    getCategoryList();
    getTagList();
  }, [getCategoryList, getTagList]);

  return (
    <div>
      <Paper>
        <div style={{ padding: "16px" }}>
          <span style={{ fontWeight: 600 }}>
            {allCategories[globals.category].name}
            <CategoryList
              categories={allCategories}
              onSelect={id => {
                setGlobals({ ...globals, category: id });
              }}
            />
          </span>
          <CreateTag type="category" handleSave={createCategory} />
          <div className={classes.spacer}>
            <DateSelect
              onChange={moment => {
                setGlobals({ ...globals, eventDate: moment.toDate() });
              }}
              value={globals.eventDate}
            />
          </div>
          <div className={classes.spacer} style={{ display: "flex" }}>
            <TagSelect
              onChange={e => {
                setGlobals({ ...globals, tags: e.target.value });
              }}
              value={globals.tags}
              availableTags={allTags}
            />
            <CreateTag type="tag" handleSave={createTag} />
          </div>
        </div>
      </Paper>
      <div className={classes.spacer}>
        <UploadButton onChange={handleFileSelect} />
      </div>
      <div className={classes.spacer}>
        <UploadList items={uploads} />
      </div>
      <div className={classes.spacer}>
        <Button
          variant={"contained"}
          color={"primary"}
          onClick={() => {
            handleUpload();
            console.log(uploads);
          }}
        >
          Upload starten
        </Button>
      </div>
    </div>
  );
}

const mapState = state => {
  return {
    allTags: getTags(state),
    allCategories: getCategories(state)
  };
};

const mapDispatch = {
  getCategoryList: getCategoryList,
  getTagList: getTagList,
  startUploads: startUploads,
  addUpload: addUpload,
  addEntities: addEntities,
  createTag: createTag,
  createCategory: createCategory
};

export default connect(
  mapState,
  mapDispatch
)(UploadForm);

// helper
function getFormDatafromObject(data) {
  const form = new FormData();
  for (const key in data) {
    form.append(key, data[key]);
  }
  return form;
}
