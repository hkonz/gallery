import React from "react";
import { makeStyles, withStyles } from "@material-ui/core/styles";
import ExpansionPanel from "@material-ui/core/ExpansionPanel";
import MuiExpansionPanelSummary from "@material-ui/core/ExpansionPanelSummary";
import ExpansionPanelDetails from "@material-ui/core/ExpansionPanelDetails";
import Typography from "@material-ui/core/Typography";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import LinearProgress from "@material-ui/core/LinearProgress";
import getHumanFileSize from "../../helpers/getHumanFileSize";
import StateIcon from "./StateIcon";
import EditForm from "../LightBox/EditForm";

const ExpansionPanelSummary = withStyles(theme => ({
  root: {
    padding: "0 " + theme.spacing(2) + "px",
    transition: "all 150ms cubic-bezier(0.4, 0, 0.2, 1) 0ms",
    "&$expanded": {
      padding: "0 " + theme.spacing(2) + "px",
      "& div[data-preview]": {
        width: "60px",
        height: "60px",
        marginRight: theme.spacing(2)
      }
    }
  },
  content: {
    margin: theme.spacing(1) + "px 0",
    "&$expanded": {
      margin: theme.spacing(2) + "px 0"
    }
  },
  expanded: {}
}))(MuiExpansionPanelSummary);

const useStyles = makeStyles(theme => ({
  root: {
    width: "100%"
  },
  heading: {
    fontSize: theme.typography.pxToRem(15),
    fontWeight: theme.typography.fontWeightRegular,
    marginRight: theme.spacing(2)
  },
  preview: {
    width: "40px",
    height: "40px",
    backgroundColor: theme.palette.grey[300],
    marginRight: theme.spacing(1),
    transition: "all 150ms cubic-bezier(0.4, 0, 0.2, 1) 0ms"
  },
  summary: {
    flexGrow: 1
  },
  infoText: {
    justifyContent: "space-between",
    display: "flex",
    flexGrow: 1,
    flexWrap: "wrap",
    width: "100%"
  },
  fileSize: {
    display: "flex"
  },
  progress: {
    flexGrow: 1,
    width: "100%"
  }
}));

function UploadList(props) {
  const { items } = props;
  const itemList = Object.keys(items);
  const classes = useStyles();

  const list = itemList.map(id => {
    const item = items[id];
    return <UploadListItem key={id} {...item} />;
  });

  return <div className={classes.root}>{list}</div>;
}

function UploadListItem(props) {
  const { id, progress, state, file, preview } = props;
  const classes = useStyles();

  return (
    <ExpansionPanel expanded={false}>
      <ExpansionPanelSummary
        aria-controls={`${id}-content`}
        id={`${id}-header`}
        className={classes.root}
      >
        <div className={classes.summary}>
          <div className={classes.infoText}>
            <Typography className={classes.heading}>{file.name}</Typography>
            <div className={classes.fileSize}>
              <Typography className={classes.heading}>
                {getHumanFileSize(file.size)}
              </Typography>
              <StateIcon state={state} />
            </div>
          </div>
          <div className={classes.progress}>
            <LinearProgress
              variant={"determinate"}
              value={progress}
              color={state === "completed" ? "secondary" : "primary"}
            />
          </div>
        </div>
      </ExpansionPanelSummary>
      <ExpansionPanelDetails>
        {/* {state === "completed" && <EditForm id={id} />} */}
      </ExpansionPanelDetails>
    </ExpansionPanel>
  );
}

export default UploadList;
