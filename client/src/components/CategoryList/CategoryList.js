import React, { useState } from "react";
import PropTypes from "prop-types";
import IconButton from "@material-ui/core/IconButton";
import Menu from "@material-ui/core/Menu";
import Divider from "@material-ui/core/Divider";
import MenuItem from "@material-ui/core/MenuItem";
import Icon from "@mdi/react";
import sortBy from "lodash/sortBy";
import { mdiFolderEditOutline } from "@mdi/js";
import MenuItemCreate from "./MenuItemCreate";
import IfAllGranted from "../UI/IfAllGranted";
import roles from "../../constants/roles";

function CategoryList(props) {
  const {
    categories,
    onSelect,
    iconColor = "inherit",
    iconSize = "24px"
  } = props;
  const [anchorEl, setAnchorEl] = useState(null);
  const [popOverAnchor, setPopOverAnchor] = useState(null);
  const [disabled, setDisabled] = useState(false);

  function handleClick(event) {
    setAnchorEl(event.currentTarget);
  }

  function handleClose(event) {
    setAnchorEl(null);
  }

  function openPopover(event) {
    setPopOverAnchor(event.currentTarget);
    setDisabled(true);
  }

  function closePopover(event) {
    setPopOverAnchor(null);
    setDisabled(false);
  }

  function handleItemClick(categoryId) {
    return function() {
      setAnchorEl(null);
      onSelect(categoryId);
    };
  }

  const categoryOrder = sortBy(categories, ["sequence"]);
  const categoryList = categoryOrder.map(category => {
    return (
      <MenuItem
        key={category.id}
        value={category.id}
        onClick={handleItemClick(category.id)}
        disabled={disabled}
      >
        {category.name}
      </MenuItem>
    );
  });

  return (
    <span>
      <IconButton onClick={handleClick} variant="contained">
        <Icon path={mdiFolderEditOutline} size={iconSize} color={iconColor} />
      </IconButton>

      <Menu
        id="simple-menu"
        anchorEl={anchorEl}
        keepMounted
        open={Boolean(anchorEl)}
        onClose={handleClose}
        disabled={disabled}
      >
        <IfAllGranted roles={roles.CATEGORY_CREATE}>
          <MenuItemCreate onClose={closePopover} onOpen={openPopover} />
          <Divider />
        </IfAllGranted>

        {categoryList}
      </Menu>
    </span>
  );
}

CategoryList.propTypes = {
  categories: PropTypes.object.isRequired,
  onSelect: PropTypes.func.isRequired,
  iconSize: PropTypes.string,
  iconColor: PropTypes.string
};

export default CategoryList;
