import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { createCategory } from "../../actions/category.actions";
import { createTag } from "../../actions/tag.actions";
import { makeStyles } from "@material-ui/core/styles";
import Popover from "@material-ui/core/Popover";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import IconButton from "@material-ui/core/IconButton";
import TextField from "@material-ui/core/TextField";
import MenuItem from "@material-ui/core/MenuItem";
import Icon from "@mdi/react";
import { mdiPlus } from "@mdi/js";
import { display } from "@material-ui/system";

const useStyles = makeStyles(theme => ({
  popoverContent: {
    padding: theme.spacing(2),
    display: "flex"
  },
  textField: {
    margin: 0,
    marginRight: theme.spacing(1)
  }
}));

const MenuItemCreate = React.forwardRef((props, ref) => {
  const {
    createCategory,
    createTag,
    onClose,
    onOpen,
    type = "category"
  } = props;
  const classes = useStyles();
  const [anchorEl, setAnchorEl] = React.useState(null);
  const [values, setValues] = React.useState({ name: "" });

  function handleClick(event) {
    setAnchorEl(event.currentTarget);
  }

  function handleClose(e) {
    setAnchorEl(null);
    onClose();
  }

  const handleChange = name => event => {
    setValues({ ...values, [name]: event.target.value });
  };

  function handleSave() {
    if (type === "tag") {
      createTag(values);
    } else if (type === "category") {
      createCategory(values);
    }
  }

  const open = Boolean(anchorEl);
  const id = open ? "simple-popover" : undefined;

  return (
    <span>
      {/* <IconButton
        aria-describedby={id}
        variant="contained"
        onClick={handleClick}
      >
        <Icon path={mdiPlus} size={"24px"} />
      </IconButton> */}

      <MenuItem key={"create"} onClick={handleClick}>
        <Icon path={mdiPlus} size={"24px"} /> Hinzufügen
      </MenuItem>

      <Popover
        id={id}
        open={open}
        onEnter={onOpen}
        anchorEl={anchorEl}
        onClose={handleClose}
        anchorOrigin={{
          vertical: "bottom",
          horizontal: "center"
        }}
        transformOrigin={{
          vertical: "top",
          horizontal: "center"
        }}
      >
        <div className={classes.popoverContent}>
          <TextField
            id="outlined-name"
            label="Name"
            className={classes.textField}
            value={values.name}
            onChange={handleChange("name")}
            margin="normal"
            variant="outlined"
          />
          <Button
            aria-describedby={id}
            variant="outlined"
            color="primary"
            onClick={() => handleSave(values)}
          >
            Ok
          </Button>
        </div>
      </Popover>
    </span>
  );
});

MenuItemCreate.propTypes = {
  type: PropTypes.string
};

const mapDispatch = {
  createCategory,
  createTag
};

export default connect(
  null,
  mapDispatch
)(MenuItemCreate);
