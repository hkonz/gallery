import React, { useEffect } from "react";
import { connect } from "react-redux";
import { getItems } from "../../actions/item.actions";
import Grid from "./Grid";
import "./Grid.css";
import SortByTabs from "../UI/SortByTabs";

import {
  getGroup,
  getGroups,
  getItemsByGroup,
  getItemsRequestActive
} from "../../selectors";

import Divider from "@material-ui/core/Divider";
import Container from "@material-ui/core/Container";
import { makeStyles } from "@material-ui/core/styles";
import NextButton from "./NextButton";
import TagFilter from "./TagFilter";
import ControlsBar from "../UI/ControlsBar";
import LightBox from "../LightBox/LightBox";
import LoadingCircle from "./LoadingCircle";

const useStyles = makeStyles(theme => ({
  controls: {
    backgroundColor: "white",
    boxShadow: theme.shadows[2]
  },
  gridArea: {
    textAlign: "center",
    paddingBottom: theme.spacing(4)
  }
}));

function GridContainer(props) {
  const { itemGroups, getItems, isItemsRequestActive } = props;
  const classes = useStyles();

  const isInitialItemsRequest = isItemsRequestActive && !itemGroups.length;
  useEffect(() => {
    getItems();
  }, [getItems]);

  return (
    <React.Fragment>
      <div className={classes.controls}>
        <Container>
          <ControlsBar />
        </Container>
      </div>
      <Container>
        <TagFilter />
        <div className={classes.gridArea}>
          {isInitialItemsRequest ? (
            <LoadingCircle />
          ) : (
            <Grid itemGroups={itemGroups} />
          )}
          <NextButton />
        </div>
      </Container>
      <LightBox />
    </React.Fragment>
  );
}

const mapState = state => {
  return {
    selectedGroup: getGroup(state),
    groups: getGroups(state),
    itemGroups: getItemsByGroup(state),
    isItemsRequestActive: getItemsRequestActive(state)
  };
};

const mapDispatchToProps = {
  getItems
};

export default connect(
  mapState,
  mapDispatchToProps
)(GridContainer);
