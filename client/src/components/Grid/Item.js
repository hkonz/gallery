import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import { API_ROOT } from "../../config/config";
import {
  getFilesPerItem,
  getItem,
  getIsSelectionModeActive,
  getSelectedItems
} from "../../selectors";
import { openLightBox, selectItem } from "../../actions/ui.actions";
import { getFileUrl } from "../../helpers/file.helpers";
import axios from "axios";
import getAuthHeader from "../../helpers/getAuthHeaders";

import PlayCircleFilledWhite from "@material-ui/icons/PlayCircleFilledWhite";

function Item(props) {
  const {
    item,
    files,
    selectedItems,
    isSelectionModeActive,
    selectItem,
    openLightBox
  } = props;
  const { original, thumb, medium } = files;
  const { type } = item;

  const style = {
    backgroundImage: `url(${getFileUrl(thumb)})`,
    backgroundPosition: "center center",
    backgroundSize: "cover",
    backgroundRepeat: "no-repeat",
    backgroundColor: "transparent"
  };

  // const [style, setStyle] = useState({
  //   backgroundImage: "none",
  //   backgroundPosition: "center center",
  //   backgroundSize: "cover",
  //   backgroundRepeat: "no-repeat",
  //   backgroundColor: "transparent"
  // });

  // useEffect(() => {
  //   (async function() {
  //     try {
  //       const { data } = await axios.get(getFileUrl(thumb), {
  //         responseType: "blob",
  //         headers: {
  //           ...getAuthHeader().headers
  //         }
  //       });
  //       setStyle({
  //         ...style,
  //         backgroundImage: `url(${URL.createObjectURL(data)})`
  //       });
  //     } catch (err) {
  //       // :)
  //     }
  //   })();
  // }, [thumb]);

  // todo: create fancy transition on click
  let gridItem = React.createRef();

  function handleClick() {
    if (isSelectionModeActive) {
      selectItem(item.id);
    } else {
      openLightBox(item.id);
    }
  }

  const isSelected = selectedItems.includes(item.id);

  return (
    <div
      className={`grid-item grid-item--width-${
        item.gridWidth
      } grid-item--height-${item.gridHeight}`}
      onClick={handleClick}
      ref={gridItem}
    >
      <div className={"grid-item-content-wrapper"}>
        <div
          className={
            "grid-item-content " +
            (isSelected ? "grid-item-content--selected" : "")
          }
          data-src={getFileUrl(medium ? medium : original)}
          data-alt={item.filename}
          style={style}
        >
          {type === "video" && (
            <div className={"grid-item-content-overlay"}>
              <PlayCircleFilledWhite
                style={{ color: "rgba(255,255,255,0.8)" }}
                fontSize={"large"}
              />
            </div>
          )}
        </div>
      </div>
    </div>
  );
}

const mapStateToProps = (state, ownProps) => {
  return {
    item: getItem(state, ownProps.id),
    files: getFilesPerItem(state, ownProps.id),
    isSelectionModeActive: getIsSelectionModeActive(state),
    selectedItems: getSelectedItems(state)
  };
};

const mapDispatch = {
  selectItem,
  openLightBox
};

export default connect(
  mapStateToProps,
  mapDispatch
)(Item);
