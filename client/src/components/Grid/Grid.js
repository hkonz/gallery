import React from "react";
import Masonry from "react-masonry-component";

import GridItem from "./Item";
import Typography from "@material-ui/core/Typography";

const masonryOptions = {
  transitionDuration: 500
};

const imagesLoadedOptions = { background: ".my-bg-image-el" };

function Grid(props) {
  const { itemGroups } = props;
  let list = [];

  itemGroups.forEach(group => {
    const { groupName, itemList } = group;
    if (itemList.length) {
      const groupHeader = (
        <div className={"grid-item grid-header"} key={groupName}>
          <Typography>
            <span className={"grid-header-content"}>{groupName}</span>
          </Typography>
        </div>
      );

      list.push(groupHeader);

      itemList.forEach(itemId => {
        list.push(<GridItem key={itemId} id={itemId} />);
      });
    }
  });

  // const list = props.items.map(id => {
  //   return <GridItem key={id} id={id} />;
  // });

  return (
    <Masonry
      className={"grid"} // default ''
      elementType={"div"} // default 'div'
      options={masonryOptions} // default {}
      disableImagesLoaded={false} // default false
      updateOnEachImageLoad={false} // default false and works only if disableImagesLoaded is false
      imagesLoadedOptions={imagesLoadedOptions} // default {}
    >
      <div className={"grid-sizer"} />
      {list}
    </Masonry>
  );
}

export default Grid;
