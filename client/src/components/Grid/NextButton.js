import React from "react";
import { connect } from "react-redux";
import Button from "@material-ui/core/Button";

import { getLimit, getPage, getTotal, getItemsCount } from "../../selectors";
import { getItems } from "../../actions/item.actions";
import { setPage } from "../../actions/group.actions";

function NextButton(props) {
  const { currentPage, setPage, getItems, total, limit, itemsCount } = props;

  if (!total || itemsCount >= total) return null;

  return (
    <Button
      variant="outlined"
      disabled={currentPage * limit > total}
      onClick={() => {
        setPage(currentPage + 1);
        getItems();
      }}
    >
      Weitere laden...
    </Button>
  );
}

const mapState = state => {
  return {
    currentPage: getPage(state),
    total: getTotal(state),
    limit: getLimit(state),
    itemsCount: getItemsCount(state)
  };
};

const mapDispatch = {
  getItems: getItems,
  setPage: setPage
};

export default connect(
  mapState,
  mapDispatch
)(NextButton);
