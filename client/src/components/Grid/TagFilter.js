import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Chip from "@material-ui/core/Chip";
import sortBy from "lodash/sortBy";
import { connect } from "react-redux";
import { addTagFilter, removeTagFilter } from "../../actions/group.actions";
import { getFilters, getTags } from "../../selectors";

const useStyles = makeStyles(theme => ({
  root: {
    paddingTop: theme.spacing(1)
  },
  chip: {
    margin: theme.spacing(0.5)
  }
}));

function TagFilter(props) {
  const { addFilter, removeFilter, tags, activeTags } = props;
  const classes = useStyles();
  const tagList = sortBy(tags, "name");

  return (
    <div className={classes.root}>
      {tagList.map(tag => {
        const isActive = activeTags.includes(tag.id);
        if (isActive) {
          return (
            <Chip
              key={tag.id}
              label={tag.name}
              onDelete={() => removeFilter(tag.id)}
              className={classes.chip}
              color="primary"
            />
          );
        } else {
          return (
            <Chip
              key={tag.id}
              label={tag.name}
              className={classes.chip}
              component="a"
              clickable
              onClick={() => addFilter(tag.id)}
              variant="outlined"
            />
          );
        }
      })}
    </div>
  );
}

const mapSate = state => {
  return {
    tags: getTags(state),
    activeTags: getFilters(state).tags
  };
};

const mapDispatch = {
  addFilter: addTagFilter,
  removeFilter: removeTagFilter
};

export default connect(
  mapSate,
  mapDispatch
)(TagFilter);
