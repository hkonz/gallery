import React, { Component } from "react";
import SwipeableViews from "react-swipeable-views";
import Item from "./Item";

export default class SwipeableItem extends Component {
  constructor() {
    super();

    this.handleItemOnLoad = this.handleItemOnLoad.bind(this);
  }

  componentDidUpdate() {
    this.swipeableActions.updateHeight();
  }

  handleItemOnLoad() {
    this.swipeableActions.updateHeight();
  }

  render() {
    const {
      activeItemIndex,
      activeItemId,
      itemsOrder,
      changeSelectedItem,
      editable
    } = this.props;

    return (
      <SwipeableViews
        index={activeItemIndex}
        enableMouseEvents
        animateHeight
        disabled={editable}
        onChangeIndex={newIndex => {
          changeSelectedItem(itemsOrder[newIndex]);
        }}
        action={actions => {
          this.swipeableActions = actions;
        }}
      >
        {itemsOrder.map(id => {
          return (
            <Item
              id={id}
              key={id}
              handleOnLoad={this.handleItemOnLoad}
              setSwipeable={this.setSwipeable}
              editable={editable}
              activeItemId={activeItemId}
            />
          );
        })}
      </SwipeableViews>
    );
  }
}
