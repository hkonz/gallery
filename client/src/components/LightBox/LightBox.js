import React from "react";
// import PropTypes from "prop-types";
import Button from "@material-ui/core/Button";
import IconButton from "@material-ui/core/IconButton";
import Icon from "@mdi/react";
import { mdiDownload } from "@mdi/js";
import { useTheme } from "@material-ui/core/styles";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import withMobileDialog from "@material-ui/core/withMobileDialog";
import MobileStepper from "@material-ui/core/MobileStepper";
import KeyboardArrowLeft from "@material-ui/icons/KeyboardArrowLeft";
import KeyboardArrowRight from "@material-ui/icons/KeyboardArrowRight";

import EditForm from "./EditForm";
import {
  getFilesPerItem,
  getItem,
  getItems,
  getItemsInGroupOrder,
  getLightBoxActive,
  getLightBoxItem,
  getLightBoxItemIds
} from "../../selectors";
import { closeLightBox, changeItem } from "../../actions/ui.actions";
import { connect } from "react-redux";
import SwipeableItem from "./SwipeableItem";
import { getFileToShow, getFileUrl } from "../../helpers/file.helpers";

function ResponsiveDialog(props) {
  const {
    fullScreen,
    itemsOrder,
    isOpen,
    activeItemId,
    activeItemFiles,
    closeLightBox,
    lightBoxItemIds,
    changeSelectedItem
  } = props;
  const theme = useTheme();
  const { previous, next } = lightBoxItemIds;

  const activeItemIndex = itemsOrder.indexOf(activeItemId);

  return (
    <Dialog
      fullScreen={fullScreen}
      fullWidth={true}
      open={isOpen}
      onClose={closeLightBox}
      aria-labelledby="responsive-dialog-title"
      scroll={"body"}
    >
      {" "}
      <DialogActions>
        <IconButton
          color="primary"
          href={getFileUrl(getFileToShow(activeItemFiles))}
          download
        >
          <Icon
            path={mdiDownload}
            size={"18px"}
            color={theme.palette.primary.main}
          />
        </IconButton>
        <Button onClick={closeLightBox} color="primary">
          Schließen
        </Button>
      </DialogActions>
      <SwipeableItem
        activeItemId={activeItemId}
        activeItemIndex={activeItemIndex}
        itemsOrder={itemsOrder}
        changeSelectedItem={changeSelectedItem}
      />
      <EditForm id={activeItemId} />
      <MobileStepper
        steps={itemsOrder.length}
        position="static"
        variant="text"
        activeStep={activeItemIndex}
        nextButton={
          <Button
            size="small"
            onClick={() => changeSelectedItem(next)}
            disabled={activeItemIndex === itemsOrder.length - 1}
          >
            Weiter
            <KeyboardArrowRight />
          </Button>
        }
        backButton={
          <Button
            size="small"
            onClick={() => changeSelectedItem(previous)}
            disabled={activeItemIndex === 0}
          >
            <KeyboardArrowLeft />
            Zurück
          </Button>
        }
      />
    </Dialog>
  );
}

const mapState = state => {
  return {
    itemsOrder: getItemsInGroupOrder(state),
    activeItemId: getLightBoxItem(state),
    isOpen: getLightBoxActive(state),
    activeItem: getItem(state, getLightBoxItem(state)),
    activeItemFiles: getFilesPerItem(state, getLightBoxItem(state)),
    items: getItems(state),
    lightBoxItemIds: getLightBoxItemIds(state)
  };
};

const mapDispatch = dispatch => {
  return {
    closeLightBox: () => dispatch(closeLightBox()),
    changeSelectedItem: id => dispatch(changeItem(id))
  };
};

export default connect(
  mapState,
  mapDispatch
)(withMobileDialog()(ResponsiveDialog));
