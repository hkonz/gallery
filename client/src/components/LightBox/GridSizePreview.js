import React from "react";

import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles(theme => ({
  wrapper: {
    position: "relative",
    width: "48px",
    height: "48px",
    overflow: "hidden"
  },
  item: {
    backgroundColor: theme.palette.grey[300],
    float: "left",
    width: "12px",
    height: "12px",
    border: "1px solid white"
  },
  itemPreview: props => ({
    backgroundColor: theme.palette.primary.main,
    width: `${props.gridWidth * 12}px`,
    height: `${props.gridHeight * 12}px`
  })
}));
export default function GridSizePreview(props) {
  const { gridWidth, gridHeight } = props;
  const classes = useStyles(props);

  const fillCount = 16 - gridWidth * gridHeight;
  let fill = [];
  for (let i = 0; i < fillCount; i++) {
    fill.push(<div key={i} className={classes.item} />);
  }

  return (
    <div className={classes.wrapper}>
      <div className={[classes.item, classes.itemPreview].join(" ")} />
      <div className={classes.item} />
      <div className={classes.item} />
      <div className={classes.item} />
      <div className={classes.item} />
      <div className={classes.item} />
      <div className={classes.item} />
      <div className={classes.item} />
      <div className={classes.item} />
      <div className={classes.item} />
      <div className={classes.item} />
      <div className={classes.item} />
      <div className={classes.item} />
      <div className={classes.item} />
      <div className={classes.item} />
      <div className={classes.item} />
    </div>
  );
}
