import React, { useState, useEffect } from "react";

function placeholderSrc(width, height) {
  return `data:image/svg+xml,%3Csvg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 ${width} ${height}"%3E%3C/svg%3E`;
}

function LazyLoadingImage(props) {
  const { url, width, height, alt = "not found", handleOnLoad } = props;
  const [state, setState] = useState({
    loaded: false,
    src: placeholderSrc(width, height)
  });

  useEffect(() => {
    setState({
      ...state,
      src: url
    });
  }, [url, state]);

  return (
    <React.Fragment>
      <img
        src={state.src}
        width={"100%"}
        data-src={url}
        alt={alt}
        onLoad={handleOnLoad}
      />
    </React.Fragment>
  );
}

export default LazyLoadingImage;
