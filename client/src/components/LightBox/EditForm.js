import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import { makeStyles } from "@material-ui/core/styles";
import { red } from "@material-ui/core/colors";
import TextField from "@material-ui/core/TextField";
import Slider from "@material-ui/lab/Slider";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import GridSizePreview from "./GridSizePreview";
import { putItemUpdate, deleteItem } from "../../actions/item.actions";
import DateString from "../UI/Date";
import Chip from "@material-ui/core/Chip";
import CategoryList from "../CategoryList/CategoryList";
import TagSelect from "./TagSelect";
import DateSelect from "./DateSelect";
import {
  getTags,
  getCategories,
  getFilesPerItem,
  getItem,
  getItemCategory,
  getItemTags
} from "../../selectors";
import IfAllGranted from "../UI/IfAllGranted";
import roles from "../../constants/roles";
import { getCategoryList } from "../../actions/category.actions";
import { getTagList } from "../../actions/tag.actions";

const useStyles = makeStyles(theme => ({
  editForm: {
    padding: theme.spacing(2),
    paddingTop: theme.spacing(1.5),
    overflow: "hidden",
    marginBottom: theme.spacing(2)
  },
  category: {
    marginBottom: theme.spacing(2),
    fontWeight: theme.typography.fontWeightBold,
    fontSize: "1.25rem"
  },
  date: {
    marginBottom: theme.spacing(1)
  },
  datePicker: {
    marginTop: "-3px"
  },
  tags: {
    marginBottom: theme.spacing(1)
  },
  comment: {
    width: "100%",
    marginTop: 0
  },
  item: {
    padding: theme.spacing(2) + " " + theme.spacing(4)
  },
  justifyContent: {
    display: "flex",
    justifyContent: "space-between"
  },
  deleteButton: {
    marginLeft: "4px"
  }
}));

const marks = [
  { value: 1, label: 1 },
  { value: 2, label: 2 },
  { value: 3, label: 3 }
];

function getAriaValueText(val) {
  return val;
}

function EditForm(props) {
  const {
    id,
    comment,
    gridWidth,
    gridHeight,
    deleteItem,
    // removeItem,
    // closeLightBox,
    tags = [],
    availableTags,
    eventDate,
    createdAt,
    getTagList,
    categories,
    categoryId,
    getCategoryList
  } = props;
  const initialState = {
    id,
    comment,
    gridWidth,
    gridHeight,
    categoryId,
    eventDate: eventDate || createdAt,
    tags: tags.map(tag => tag.id)
  };
  const [values, setValues] = useState(initialState);
  const [editEnabled, setEditEnabled] = useState(false);
  const classes = useStyles();

  useEffect(() => {
    setValues(initialState);
  }, [id]);

  useEffect(() => {
    getCategoryList();
    getTagList();
  }, [getCategoryList, getTagList]);

  const handleChange = name => event => {
    setValues({ ...values, [name]: event.target.value });
  };

  const handleSliderChange = name => (event, value) => {
    setValues({ ...values, [name]: parseInt(value) });
  };

  const handleCategoryChange = id => {
    setValues({ ...values, categoryId: id });
  };

  const handleEventDateChange = moment => {
    setValues({ ...values, eventDate: moment.toDate() });
  };

  const handleSubmit = () => {
    console.log("handle submit");
    props.onSubmit({
      id,
      ...values
    });
  };

  return (
    <div className={classes.editForm}>
      <div className={classes.justifyContent}>
        <div>
          {" "}
          <div className={classes.category}>
            {categoryId ? categories[values.categoryId].name : ""}
            {editEnabled && (
              <CategoryList
                onSelect={handleCategoryChange}
                categories={categories}
              />
            )}
          </div>
        </div>
        <div>
          <IfAllGranted roles={roles.ITEM_UPDATE}>
            <Button
              variant="outlined"
              onClick={() => {
                setEditEnabled(!editEnabled);
              }}
            >
              Bearbeiten
            </Button>
          </IfAllGranted>
        </div>
      </div>

      <div className={classes.date}>
        {editEnabled ? (
          <DateSelect
            value={values.eventDate}
            onChange={handleEventDateChange}
          />
        ) : (
          <DateString date={values.eventDate} />
        )}
      </div>
      <div className={classes.tags}>
        {editEnabled ? (
          <TagSelect
            onChange={handleChange("tags")}
            availableTags={availableTags}
            value={values.tags}
          />
        ) : (
          values.tags.map(tagId => {
            return <Chip key={tagId} label={availableTags[tagId].name} />;
          })
        )}
      </div>
      {editEnabled && (
        <div>
          <TextField
            id="outlined-multiline-flexible"
            name="description"
            label="Kommentar"
            multiline
            rowsMax="5"
            value={values.comment}
            onChange={handleChange("comment")}
            className={classes.comment}
            margin="normal"
            variant="outlined"
          />
          <Grid container spacing={6}>
            <Grid item xs={4} className={classes.item}>
              <Slider
                defaultValue={values.gridWidth}
                getAriaValueText={getAriaValueText}
                max={3}
                min={1}
                step={1}
                value={values.gridWidth}
                marks={marks}
                onChange={handleSliderChange("gridWidth")}
              />
            </Grid>
            <Grid item xs={4} className={classes.item}>
              <Slider
                defaultValue={values.gridHeight}
                getAriaValueText={getAriaValueText}
                max={3}
                min={1}
                step={1}
                value={values.gridHeight}
                marks={marks}
                onChange={handleSliderChange("gridHeight")}
              />
            </Grid>
            <Grid item xs={4}>
              <GridSizePreview
                gridWidth={values.gridWidth}
                gridHeight={values.gridHeight}
              />
            </Grid>
          </Grid>
          <div>
            <Button onClick={handleSubmit} variant="outlined" color="primary">
              Speichern
            </Button>
            <IfAllGranted roles={roles.ITEM_DELETE}>
              <Button
                variant="text"
                onClick={() => deleteItem(id)}
                className={classes.deleteButton}
              >
                Löschen
              </Button>
            </IfAllGranted>
          </div>
        </div>
      )}
    </div>
  );
}

const mapState = (state, ownProps) => {
  return {
    ...getItem(state, ownProps.id),
    files: getFilesPerItem(state, ownProps.id),
    category: getItemCategory(state, ownProps.id),
    tags: getItemTags(state, ownProps.id),
    availableTags: getTags(state),
    categories: getCategories(state)
  };
};

const mapDispatch = {
  onSubmit: putItemUpdate,
  deleteItem: deleteItem,
  getCategoryList: getCategoryList,
  getTagList: getTagList
};

export default connect(
  mapState,
  mapDispatch
)(EditForm);
