import React from "react";
import PropTypes from "prop-types";
import { DatePicker } from "@material-ui/pickers";

function DateSelect(props) {
  const { onChange, value } = props;
  return (
    <DatePicker
      label="Ereignisdatum"
      format="LL"
      value={value}
      cancelLabel="Abbrechen"
      onChange={onChange}
      animateYearScrolling
      inputVariant="outlined"
      style={{ width: "100%" }}
    />
  );
}

DateSelect.propTypes = {
  onChange: PropTypes.func.isRequired
};

export default DateSelect;
