import React from "react";
import { connect } from "react-redux";
import { API_ROOT } from "../../config/config";
import {
  getFilesPerItem,
  getItem,
  getItemCategory,
  getItemTags
} from "../../selectors";
import { makeStyles } from "@material-ui/core/styles";
import Player from "react-player";
import LazyLoadingImage from "./LazyLoadingImage";
import { putItemUpdate, removeItem } from "../../actions/item.actions";
import { closeLightBox } from "../../actions/ui.actions";
import { getFileToShow, getFileUrl } from "../../helpers/file.helpers";

const useStyle = makeStyles(theme => {
  return {
    comment: {
      padding: theme.spacing(2),
      paddingTop: theme.spacing(1.5),
      fontWeight: theme.typography.fontWeightBold,
      borderBottom: "1px solid " + theme.palette.divider
    },

    playerWrapper: fileToShow => ({
      maxHeight: "800px",
      backgroundColor: "rgba(0, 0, 0, 0.8)"
    })
  };
});

function Item(props) {
  const { files, comment, handleOnLoad, type } = props;

  const fileToShow = getFileToShow(files);
  const classes = useStyle(fileToShow);

  return (
    <div>
      {type === "image" && (
        <LazyLoadingImage
          url={getFileUrl(fileToShow)}
          width={fileToShow.width}
          height={fileToShow.height}
          alt={comment}
          handleOnLoad={handleOnLoad}
        />
      )}
      {type === "video" && (
        <div className={classes.playerWrapper}>
          <Player
            url={getFileUrl(fileToShow)}
            controls={true}
            width="100%"
            playing={false}
          />
        </div>
      )}
      {comment && <div className={classes.comment}>{comment}</div>}
    </div>
  );
}

const mapState = (state, ownProps) => {
  return {
    ...getItem(state, ownProps.id),
    files: getFilesPerItem(state, ownProps.id),
    category: getItemCategory(state, ownProps.id),
    tags: getItemTags(state, ownProps.id)
  };
};

const mapDispatch = {
  putItemUpdate: putItemUpdate,
  removeItem,
  closeLightBox
};

export default connect(
  mapState,
  mapDispatch
)(Item);
