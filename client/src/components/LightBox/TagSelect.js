import React from "react";
import PropTypes from "prop-types";
import { makeStyles, withStyles } from "@material-ui/core/styles";
import OutlinedInput from "@material-ui/core/OutlinedInput";
import InputLabel from "@material-ui/core/InputLabel";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import FormControl from "@material-ui/core/FormControl";
import Chip from "@material-ui/core/Chip";
import _sortBy from "lodash/sortBy";

const useStyle = makeStyles(theme => {
  return {
    formControl_root: {
      width: "100%"
    },
    select: {
      whiteSpace: "inherit",

      "&:focus": {
        backgroundColor: "transparent"
      }
    },
    outlinedInput: props => ({
      padding: props.padding || "12px 14px",
      minHeight: props.minHeight || "32px"
    }),
    chip: {
      marginRight: theme.spacing(0.5),
      marginBottom: theme.spacing(0.5)
    }
  };
});

const MenuProps = {
  anchorOrigin: {
    vertical: "bottom",
    horizontal: "center"
  },
  transformOrigin: {
    vertical: "top",
    horizontal: "center"
  }
};

function TagSelect(props) {
  const { onChange, value, availableTags } = props;
  const tagList = _sortBy(availableTags, ["name"]);

  const inputLabel = React.useRef(null);
  const [labelWidth, setLabelWidth] = React.useState(0);
  React.useEffect(() => {
    setLabelWidth(inputLabel.current.offsetWidth);
  }, []);
  const classes = useStyle(props);
  return (
    <FormControl
      variant="outlined"
      classes={{ root: classes.formControl_root }}
    >
      <InputLabel
        variant="outlined"
        htmlFor="select-multiple-chip"
        ref={inputLabel}
      >
        Tags
      </InputLabel>
      <Select
        multiple
        variant="outlined"
        value={value}
        onChange={onChange}
        classes={{
          select: classes.select // class name, e.g. `classes-nesting-root-x`
        }}
        input={
          <OutlinedInput
            id="select-multiple-chip"
            variant="outlined"
            labelWidth={labelWidth}
            classes={{ input: classes.outlinedInput }}
          />
        }
        renderValue={selected =>
          selected.map(tagId => (
            <Chip
              key={tagId}
              label={availableTags[tagId].name}
              className={classes.chip}
            />
          ))
        }
        MenuProps={MenuProps}
      >
        {tagList.map(it => (
          <MenuItem key={it.id} value={it.id}>
            {it.name}
          </MenuItem>
        ))}
      </Select>
    </FormControl>
  );
}

TagSelect.propTypes = {
  onChange: PropTypes.func.isRequired,
  availableTags: PropTypes.object.isRequired,
  value: PropTypes.array.isRequired,
  minHeight: PropTypes.string
};

export default TagSelect;
