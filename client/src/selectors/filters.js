import filter from "lodash/filter";

export function filterTags(items, tags) {
  return filter(items, function(item) {
    return tags.every(tag => item.tags.includes(tag));
  });
}

export function filterTypes(items, types, files) {
  return filter(items, function(item) {
    if (!types.length) return true;
    return types.some(type => type.startsWith(item.type));
  });
}
