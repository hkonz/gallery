import groupBy from "lodash/groupBy";
const DELIMITER = "_||_";

const monthNames = [
  "Januar",
  "Februar",
  "März",
  "April",
  "Mai",
  "Juni",
  "Juli",
  "August",
  "September",
  "Oktober",
  "November",
  "Dezember"
];

const createArrayFromGroups = (groupedItems, sortFn) => {
  const result = [];
  const groupNames = Object.keys(groupedItems);
  const groupSorted = groupNames.sort(sortFn);

  groupSorted.forEach(groupName => {
    const groupObject = {
      groupName: groupName.split(DELIMITER)[1],
      itemList: groupedItems[groupName].map(item => item.id)
    };
    result.push(groupObject);
  });
  return result;
};

const categories = (items, categories) => {
  const groupedByCategories = groupBy(items, item => {
    const { categoryId } = item;

    if (categoryId && categories[categoryId]) {
      const category = categories[categoryId];
      return category.sequence + DELIMITER + category.name;
    } else {
      return "Z" + DELIMITER + "Ohne Kategorie";
    }
  });

  return createArrayFromGroups(groupedByCategories);
};

const years = items => {
  const groupedItems = groupBy(items, item => {
    const { createdAt, eventDate } = item;
    const dateToCheck = eventDate ? new Date(eventDate) : new Date(createdAt);

    const year = dateToCheck.getFullYear();
    return "" + year + DELIMITER + year;
  });

  return createArrayFromGroups(groupedItems, sortDesc);
};

const months = items => {
  const groupedItems = groupBy(items, item => {
    const { createdAt, eventDate } = item;
    const dateToCheck = eventDate ? new Date(eventDate) : new Date(createdAt);

    const month = dateToCheck.getMonth();
    const year = dateToCheck.getFullYear();
    return "" + year + addZ(month) + DELIMITER + monthNames[month] + " " + year;
  });

  return createArrayFromGroups(groupedItems, sortDesc);
};

const days = items => {
  const groupedItems = groupBy(items, item => {
    const { createdAt, eventDate } = item;
    const dateToCheck = eventDate ? new Date(eventDate) : new Date(createdAt);

    const day = dateToCheck.getDate();
    const month = dateToCheck.getMonth();
    const year = dateToCheck.getFullYear();
    return (
      "" +
      year +
      addZ(month) +
      addZ(day) +
      DELIMITER +
      day +
      ". " +
      monthNames[month] +
      " " +
      year
    );
  });

  return createArrayFromGroups(groupedItems, sortDesc);
};

function addZ(n) {
  return n < 10 ? "0" + n : "" + n;
}

function sortDesc(a, b) {
  if (a > b) {
    return -1;
  }
  if (a < b) {
    return 1;
  }
  return 0;
}

export default {
  categories,
  years,
  months,
  days
};
