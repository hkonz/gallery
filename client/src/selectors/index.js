import { createSelector } from "redux-starter-kit";
import groupBy from "./groupBy";
import { filterTags, filterTypes } from "./filters";
import { getSortString } from "../helpers/getSortString";

export const getItem = (state, id) => state.items.byId[id];

export const getItems = state => state.items.byId;
export const getItemsCount = state => Object.keys(getItems(state)).length;

export const getItemsRequestActive = state => !!state.items.request.fetching;

export const getCategories = state => state.categories;

export const getCategoriesInAction = createSelector(
  [getItems, getCategories],
  (items, categories) => {
    const groupedByCategoryId = groupBy(items, "categoryId");
    const categoryIds = Object.keys(groupedByCategoryId);

    return categoryIds.map(categoryId => categories[categoryId]);
  }
);

export const getItemCategory = createSelector(
  [getItem, getCategories],
  (item, categories) => {
    return item ? categories[item.categoryId] : null;
  }
);

export const getTags = state => state.tags;

export const getItemTags = createSelector(
  [getItem, getTags],
  (item, tags) => {
    return item ? item.tags.map(tagId => tags[tagId]) : [];
  }
);

export const getFiles = state => state.files;

export const getGroup = state => state.group.selected;
export const getGroups = state => state.group.all;
export const getGroupsOrder = state => state.group.order;

export const getFilters = state => state.group.filters;

export const getLimit = state => state.group.limit;
export const getPage = state => state.group.page;
export const getSort = state => state.group.sort;
export const getTotal = state => state.group.total;

export const getItemQuery = createSelector(
  [getLimit, getPage, getSort],
  (limit, page, sort) => {
    const sortString = getSortString(sort);
    return `limit=${limit}&page=${page}&sort=${sortString}`;
  }
);

export const getItemsByGroup = createSelector(
  [getItems, getGroup, getCategories, getFilters],
  (items, groupType, categories, filters) => {
    const _filteredItems = filterTypes(items, filters.types);
    const filteredItems = filterTags(_filteredItems, filters.tags);
    return groupBy[groupType](filteredItems, categories);
  }
);

export const getItemsInGroupOrder = createSelector(
  [getItemsByGroup],
  groups => {
    const result = [];
    groups.forEach(group => {
      result.push(...group.itemList);
    });
    return result;
  }
);

export const getFilesPerItem = createSelector(
  [getItem, getFiles],
  (item, files) => {
    let result = {};
    if (item && item.files) {
      item.files.forEach(fileId => {
        const { type } = files[fileId];
        result[type] = files[fileId];
      });
    }

    return result;
  }
);

// ui
export const getLightBoxActive = state => state.ui.lightBoxActive;
export const getLightBoxItem = state => state.ui.selectedItem;
export const getSelectedItems = state => state.ui.selectedItems;
export const getIsSelectionModeActive = state => state.ui.selectionMode;
export const getActiveScreen = state => state.ui.activeScreen;

export const getLightBoxItemIds = createSelector(
  [getItemsInGroupOrder, getLightBoxItem],
  (itemOrder, itemId) => {
    let result = [];
    const idx = itemOrder.indexOf(itemId);
    if (idx < 1) {
      result = [undefined, itemOrder[0], itemOrder[1]];
    } else {
      result = itemOrder.slice(idx - 1, idx + 2);
    }
    const [previous, active, next] = result;
    return {
      previous,
      active,
      next
    };
  }
);

// auth

export const getAuth = state => state.auth;
export const getAuthUser = state => state.auth.user;

// uploads

export const getUploadList = state => state.uploads.byId;
export const getUploadById = (state, id) => state.uploads[id];
export const getUploadGlobalOptions = state => state.uploads.globalOptions;
export const getActiveUploadCount = state => state.uploads.acticeUploadCount;
