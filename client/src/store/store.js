import { configureStore } from "redux-starter-kit";
import uiReducer from "../reducers/ui.reducer";
import itemReducer from "../reducers/item.reducer";
import fileReducer from "../reducers/file.reducer";
import tagReducer from "../reducers/tag.reducer";
import categoryReducer from "../reducers/category.reducer";
import uploadsReducer from "../reducers/upload.reducer";

import groupReducer from "../reducers/group.reducer";

import authReducer from "../reducers/auth.reducer";

const store = configureStore({
  reducer: {
    items: itemReducer,
    files: fileReducer,
    tags: tagReducer,
    categories: categoryReducer,
    group: groupReducer,
    ui: uiReducer,
    auth: authReducer,
    uploads: uploadsReducer
  }
});

export default store;
