import { createReducer, combineReducers } from "redux-starter-kit";
import * as action from "../actions/item.actions";
import { addEntities } from "../actions/entities.actions";
import { loggedOut } from "../actions/auth.actions";

const initialState = {};

const requestReducer = createReducer(initialState, {
  [action.itemsRequest]: (state, action) => {
    return {
      ...state,
      fetching: true
    };
  },
  [action.itemsRequestFailure]: (state, action) => {
    return {
      ...state,
      fetching: false,
      message: action.payload.message,
      status: action.payload.status
    };
  },
  [action.itemsRequestSuccess]: (state, action) => {
    return {
      ...state,
      fetching: false,
      message: action.payload.message,
      status: action.payload.status
    };
  }
});

const byIdReducer = createReducer(initialState, {
  [addEntities]: (state, action) => {
    return {
      ...state,
      ...action.payload.items
    };
  },
  [action.addItem]: (state, action) => {
    return {
      ...state,
      [action.payload.id]: action.payload
    };
  },
  [action.addItems]: (state, action) => {
    return {
      ...state,
      ...action.payload
    };
  },
  [action.removeItem]: (state, action) => {
    delete state[action.payload];
  },
  [action.removeItems]: (state, action) => {
    action.payload.forEach(id => {
      delete state[id];
    });
  },
  [action.updateItem]: (state, action) => {
    // Can still return an immutably-updated value if we want to
    return {
      ...state,
      [action.payload.id]: {
        ...state[action.payload.id],
        ...action.payload
      }
    };
  },
  [loggedOut]: state => {
    return initialState;
  }
});

const itemsReducer = combineReducers({
  byId: byIdReducer,
  request: requestReducer
});

export default itemsReducer;
