import { createReducer } from "redux-starter-kit";

import {
  authSuccess,
  authFailure,
  authRequest,
  loggedOut
} from "../actions/auth.actions";

const initialState = {
  user: null,
  isAuthenticated: false,
  fetching: false
};

export default createReducer(initialState, {
  [authRequest]: (state, action) => {
    return {
      ...state,
      fetching: true
    };
  },
  [authSuccess]: (state, action) => {
    return {
      user: action.payload.user,
      isAuthenticated: true,
      fetching: false
    };
  },
  [authFailure]: (state, action) => {
    return {
      ...initialState,
      errorMessage: action.errorMessage
    };
  },
  [loggedOut]: (state, action) => {
    return {
      ...initialState,
      message: action.message
    };
  }
});
