import { createReducer } from "redux-starter-kit";

import {
  openLightBox,
  closeLightBox,
  changeItem,
  changeScreen,
  selectItem,
  selectItems,
  toggleSelectionMode,
  clearSelectedItems
} from "../actions/ui.actions";
import { loggedOut } from "../actions/auth.actions";

const initialState = {
  lightBoxActive: false,
  selectedItem: "",
  activeScreen: "gallery",
  selectionMode: false,
  selectedItems: []
};

export default createReducer(initialState, {
  [openLightBox]: (state, action) => {
    return {
      ...state,
      lightBoxActive: true,
      selectedItem: action.payload
    };
  },
  [closeLightBox]: (state, action) => {
    return {
      ...state,
      lightBoxActive: false
    };
  },
  [changeItem]: (state, action) => {
    return {
      ...state,
      selectedItem: action.payload
    };
  },
  [changeScreen]: (state, action) => {
    return {
      ...state,
      activeScreen: action.payload
    };
  },
  [toggleSelectionMode]: state => {
    const newState = !state.selectionMode;
    return {
      ...state,
      selectionMode: newState,
      selectedItems: newState ? state.selectedItems : []
    };
  },
  [selectItem]: (state, action) => {
    const isIncluded = state.selectedItems.includes(action.payload);
    return {
      ...state,
      selectedItems: isIncluded
        ? state.selectedItems.filter(id => id !== action.payload)
        : [...state.selectedItems, action.payload]
    };
  },
  [selectItems]: (state, action) => {
    return {
      ...state,
      selectedItems: state.selectedItems.concat(action.payload)
    };
  },
  [clearSelectedItems]: state => {
    return {
      ...state,
      selectedItems: []
    };
  },
  [loggedOut]: () => {
    return initialState;
  }
});
