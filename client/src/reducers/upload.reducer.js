import { createReducer, combineReducers } from "redux-starter-kit";
import * as actions from "../actions/upload.action";
import omit from "lodash/omit";
// import { addEntities } from "../actions/entities.actions";
import { loggedOut } from "../actions/auth.actions";

const initialState_activeUploadCount = 0;
const initialState_byId = {};
const initialState_globalOptions = {};

const byIdReducer = createReducer(initialState_byId, {
  [actions.addUpload]: (state, action) => ({
    ...state,
    [action.payload.id]: action.payload
  }),
  [actions.removeUpload]: (state, action) => omit(state, action.payload.id),
  [actions.setUploadList]: (state, action) => ({ ...action.payload }),
  [actions.updateUpload]: (state, action) => ({
    ...state,
    [action.payload.id]: { ...state[action.payload.id], ...action.payload }
  }),
  [actions.filterUploadList]: (state, action) => omit(state, action.payload),
  [actions.uploadRequest]: (state, action) => ({
    ...state,
    [action.payload]: { ...state[action.payload], status: "uploading" }
  }),
  [actions.uploadRequestFailure]: (state, action) => ({
    ...state,
    [action.payload]: { ...state[action.payload], status: "failed" }
  }),
  [actions.uploadRequestSuccess]: (state, action) => ({
    ...state,
    [action.payload]: { ...state[action.payload], status: "complete" }
  }),
  [loggedOut]: () => initialState_byId
});
const activeUploadCountReducer = createReducer(initialState_activeUploadCount, {
  [actions.uploadRequest]: state => ++state,
  [actions.uploadRequestFailure]: state => --state,
  [actions.uploadRequestSuccess]: state => --state,
  [loggedOut]: () => initialState_activeUploadCount
});
const globalOptionsReducer = createReducer(initialState_globalOptions, {
  [actions.updateGlobalOptions]: (state, action) => ({
    ...state,
    ...action.payload
  }),
  [loggedOut]: () => initialState_globalOptions
});

export default combineReducers({
  byId: byIdReducer,
  activeUploadCount: activeUploadCountReducer,
  globalOptions: globalOptionsReducer
});
