import { createReducer } from "redux-starter-kit";
import * as action from "../actions/tag.actions";
import omit from "lodash/omit";
import { addEntities } from "../actions/entities.actions";
import { loggedOut } from "../actions/auth.actions";

const initialState = {};

export default createReducer(initialState, {
  [addEntities]: (state, action) => {
    return {
      ...state,
      ...action.payload.tags
    };
  },
  [action.addTag]: (state, action) => {
    return {
      ...state,
      [action.payload.id]: action.payload
    };
  },
  [action.addTags]: (state, action) => {
    return {
      ...state,
      ...action.payload
    };
  },
  [action.removeTag]: (state, action) => {
    delete state[action.payload];
  },
  [action.removeTags]: (state, action) => {
    state = omit(state, action.payload);
  },
  [action.updateTag]: (state, action) => {
    // Can still return an immutably-updated value if we want to
    return {
      ...state,
      [action.payload.id]: {
        ...state[action.payload.id],
        ...action.payload
      }
    };
  },
  [loggedOut]: state => {
    return initialState;
  }
});
