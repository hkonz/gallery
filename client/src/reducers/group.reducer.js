import { createReducer } from "redux-starter-kit";

import {
  selectGroup,
  addTagFilter,
  addTypeFilter,
  removeTagFilter,
  removeTypeFilter,
  setPage,
  setLimit,
  setSort,
  addSortCriteria,
  removeSortCriteria,
  toggleSortCriteria,
  setTotal
} from "../actions/group.actions";
import { loggedOut } from "../actions/auth.actions";

const initialState = {
  all: [
    { name: "days", label: "Tage" },
    { name: "months", label: "Monate" },
    { name: "years", label: "Jahre" },
    { name: "categories", label: "Kategorien" }
  ],
  selected: "days",
  sort: [
    { prop: "eventDate", desc: true }
    // { prop: "category.sequence", desc: false }
  ],
  filters: {
    types: [],
    tags: []
  },
  limit: 100,
  page: 1,
  total: 0
};

export default createReducer(initialState, {
  [selectGroup]: (state, action) => {
    return {
      ...state,
      selected: action.payload
    };
  },
  [addTagFilter]: (state, action) => {
    return {
      ...state,
      filters: {
        ...state.filters,
        tags: state.filters.tags.concat(action.payload)
      }
    };
  },
  [addTypeFilter]: (state, action) => {
    return {
      ...state,
      filters: {
        ...state.filters,
        types: state.filters.types.concat(action.payload)
      }
    };
  },
  [removeTagFilter]: (state, action) => {
    return {
      ...state,
      filters: {
        ...state.filters,
        tags: state.filters.tags.filter(entry => entry !== action.payload)
      }
    };
  },
  [removeTypeFilter]: (state, action) => {
    return {
      ...state,
      filters: {
        ...state.filters,
        types: state.filters.types.filter(entry => entry !== action.payload)
      }
    };
  },
  [setPage]: (state, action) => {
    return {
      ...state,
      page: action.payload
    };
  },
  [setLimit]: (state, action) => {
    return {
      ...state,
      limit: action.payload
    };
  },
  [setSort]: (state, action) => {
    return {
      ...state,
      sort: action.payload
    };
  },
  [setTotal]: (state, action) => {
    return {
      ...state,
      total: action.payload
    };
  },
  [addSortCriteria]: (state, action) => {
    return {
      ...state,
      sort: [action.payload, ...state.sort]
    };
  },
  [removeSortCriteria]: (state, action) => {
    return {
      ...state,
      sort: state.sort.filter(criteria => criteria.prop !== action.payload)
    };
  },
  [toggleSortCriteria]: (state, action) => {
    return {
      ...state,
      sort: state.sort.map(criteria => {
        if (criteria.prop === action.payload) {
          criteria.desc = !criteria.desc;
        }
        return criteria;
      })
    };
  },
  [loggedOut]: state => {
    return initialState;
  }
});
