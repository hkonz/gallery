import { createReducer } from "redux-starter-kit";
import * as action from "../actions/category.actions";
import { addEntities } from "../actions/entities.actions";
import omit from "lodash/omit";
import { loggedOut } from "../actions/auth.actions";

const initialState = {};

export default createReducer(initialState, {
  [addEntities]: (state, action) => {
    return {
      ...state,
      ...action.payload.categories
    };
  },
  [action.addCategory]: (state, action) => {
    return {
      ...state,
      [action.payload.id]: action.payload
    };
  },
  [action.addCategories]: (state, action) => {
    return {
      ...state,
      ...action.payload
    };
  },
  [action.removeCategory]: (state, action) => {
    delete state[action.payload];
  },
  [action.removeCategories]: (state, action) => {
    state = omit(state, action.payload);
  },
  [action.updateCategory]: (state, action) => {
    // Can still return an immutably-updated value if we want to
    return {
      ...state,
      [action.payload.id]: {
        ...state[action.payload.id],
        ...action.payload
      }
    };
  },
  [loggedOut]: state => {
    return initialState;
  }
});
