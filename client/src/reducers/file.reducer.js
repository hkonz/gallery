import { createReducer } from "redux-starter-kit";
import * as action from "../actions/file.actions";
import omit from "lodash/omit";
import { addEntities } from "../actions/entities.actions";
import { loggedOut } from "../actions/auth.actions";

const initialState = {};

const fileReducer = createReducer(initialState, {
  [addEntities]: (state, action) => {
    return {
      ...state,
      ...action.payload.files
    };
  },
  [action.addFile]: (state, action) => {
    return {
      ...state,
      [action.payload.id]: action.payload
    };
  },
  [action.addFiles]: (state, action) => {
    return {
      ...state,
      ...action.payload
    };
  },
  [action.removeFile]: (state, action) => {
    delete state[action.payload];
  },
  [action.removeFiles]: (state, action) => {
    state = omit(state, action.payload);
  },
  [action.updateFile]: (state, action) => {
    // Can still return an immutably-updated value if we want to
    return {
      ...state,
      [action.payload.id]: {
        ...state[action.payload.id],
        ...action.payload
      }
    };
  },
  [loggedOut]: state => {
    return initialState;
  }
});

export default fileReducer;
