import { API_ROOT } from "../config/config";

export function getFileToShow(files) {
  const { medium, original } = files;
  if (medium) return medium;
  return original;
}

export function getFileUrl(file) {
  const prefix = API_ROOT + "/files/";
  if (file) {
    return prefix + file.id;
  } else {
    return prefix + 0;
  }
}
