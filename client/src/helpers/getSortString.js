// [{ prop: "eventDate", desc: true }]  => "eventDate+desc"
export const getSortString = arrayOfCriteria => {
  return arrayOfCriteria
    .map(criteria => `${criteria.prop}+${criteria.desc ? "desc" : "asc"}`)
    .join(",");
};
