import { normalize, schema } from "normalizr";

// FILE
const file = new schema.Entity("files");

// CATEGORY
const category = new schema.Entity("categories");

// TAGS
const tag = new schema.Entity("tags");

// ITEM
const item = new schema.Entity("items", {
  tags: [tag],
  files: [file],
  category: category
});

// multiple
export const normalizeItems = items => normalize(items, [item]);

export const normalizeTags = tags => normalize(tags, [tag]);

export const normalizeCategories = categories =>
  normalize(categories, [category]);

export const normalizeFiles = files => normalize(files, [file]);

// single
export const normalizeItem = _item => normalize(_item, item);

export const normalizeTag = _tag => normalize(_tag, tag);

export const normalizeCategory = _category => normalize(_category, category);

export const normalizeFile = _file => normalize(_file, file);
