import React from "react";
import { connect } from "react-redux";
import "./App.css";
import "./fonts/inter.css";
import { createMuiTheme } from "@material-ui/core/styles";
import { ThemeProvider } from "@material-ui/styles";
import MomentUtils from "@date-io/moment";
import { MuiPickersUtilsProvider } from "@material-ui/pickers";
import ScreenGallery from "./screens/Gallery";
import ScreenLogin from "./screens/Login";
import ScreenUpload from "./screens/Upload";

import "moment/locale/de";

import { getAuth, getActiveScreen } from "./selectors";

const palette = {
  primary: { main: "#0277bd" },
  secondary: { main: "#8BC34A" },
  background: {
    main: `linear-gradient(
      to left,
      #f3e7e9 0%,
      #e3eeff 99%,
      #e3eeff 100%
    )`
  }
};

const theme = createMuiTheme({
  palette,
  typography: {
    fontWeightBold: 600,
    fontWeightMedium: 500,
    fontWeightRegular: 400,
    fontWeightLight: 300,
    fontFamily: [
      "Inter",
      "Roboto",
      '"Helvetica Neue"',
      "Arial",
      "sans-serif",
      '"Apple Color Emoji"',
      '"Segoe UI Emoji"',
      '"Segoe UI Symbol"'
    ].join(",")
  }
});

function App(props) {
  const { isAuthenticated, activeScreen } = props;
  return (
    <ThemeProvider theme={theme}>
      <MuiPickersUtilsProvider utils={MomentUtils} locale={"de"}>
        <div className="App">
          {!isAuthenticated ? (
            <ScreenLogin />
          ) : (
            <Screens activeScreen={activeScreen} />
          )}
        </div>
      </MuiPickersUtilsProvider>
    </ThemeProvider>
  );
}

function Screens(props) {
  const { activeScreen } = props;
  return (
    <React.Fragment>
      {activeScreen === "gallery" && <ScreenGallery />}
      {activeScreen === "upload" && <ScreenUpload />}
      {activeScreen === "user" && <div>Users Screen</div>}
    </React.Fragment>
  );
}

const mapState = state => {
  return {
    ...getAuth(state),
    activeScreen: getActiveScreen(state)
  };
};

export default connect(mapState)(App);
