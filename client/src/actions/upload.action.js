import { createAction } from "redux-starter-kit";
import getAuthHeader from "../helpers/getAuthHeaders";
import { API_ROOT } from "../config/config";
import axios from "axios";
import { logout } from "./auth.actions";
import {
  getUploadList,
  getUploadById,
  getUploadGlobalOptions
} from "../selectors";

export const addUpload = createAction("UPLOAD_ADD");
export const removeUpload = createAction("UPLOAD_REMOVE");
export const updateUpload = createAction("UPLOAD_UPDATE");
export const setUploadList = createAction("UPLOAD_SET_LIST");
export const filterUploadList = createAction("UPLOAD_FILTER_LIST");

export const updateGlobalOptions = createAction("UPLOAD_GLOBAL_OPTIONS");

export const uploadRequest = createAction("UPLOAD_REQUEST");
export const uploadRequestSuccess = createAction("UPLOAD_REQUEST_SUCCESS");
export const uploadRequestFailure = createAction("UPLOAD_REQUEST_FAILURE");

export const startUploads = () => {
  return (dispatch, getState) => {
    const state = getState();
    const uploads = getUploadList(state);
    for (let id in uploads) {
      if (uploads.hasOwnProperty(id)) {
        dispatch(startUpload(id));
      }
    }
  };
};

export const startUpload = _id => {
  return async (dispatch, getState) => {
    const state = getState();
    try {
      dispatch(uploadRequest(_id));
      const upload = getUploadById(state, _id);
      const globals = getUploadGlobalOptions(state);
      const data = {
        ...globals,
        ...upload
      };

      const response = await axios.post(
        API_ROOT + "/upload",
        getFormDatafromObject(data),
        {
          headers: {
            "Content-Type": "multipart/form-data",
            ...getAuthHeader().headers
          },
          onUploadProgress: progressEvent => {
            let percentCompleted = Math.floor(
              (progressEvent.loaded * 100) / progressEvent.total
            );
            console.log("percentCompleted", percentCompleted);
          }
        }
      );
      console.log("repsonse", response);
      dispatch(
        uploadRequestSuccess({
          message: "upload succeeded",
          status: 201
        })
      );
    } catch (err) {
      const status = err.response ? err.response.status : 400;
      dispatch(
        uploadRequestFailure({
          message: err.message,
          status: status
        })
      );
      if (status === 401) dispatch(logout());
    }
  };
};

// helper
function getFormDatafromObject(data) {
  const form = new FormData();
  for (const key in data) {
    form.append(key, data[key]);
  }
  return form;
}
