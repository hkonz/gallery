import { createAction } from "redux-starter-kit";

export const selectGroup = createAction("SELECT_GROUP");

export const addTagFilter = createAction("ADD_TAG_FILTER");
export const removeTagFilter = createAction("REMOVE_TAG_FILTER");

export const addTypeFilter = createAction("ADD_TYP_FILTER");
export const removeTypeFilter = createAction("REMOVE_TYPE_FILTER");

export const setPage = createAction("SET_PAGE");
export const setLimit = createAction("SET_LIMIT");
export const setTotal = createAction("SET_TOTAL");

export const setSort = createAction("SET_SORT");
export const addSortCriteria = createAction("ADD_SORT_CRITERIA");
export const removeSortCriteria = createAction("ADD_SORT_CRITERIA");
export const toggleSortCriteria = createAction("TOGGLE_SORT_CRITERIA");
