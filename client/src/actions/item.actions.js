import { createAction } from "redux-starter-kit";
import { normalizeItem, normalizeItems } from "../helpers/normalize";
import { API_ROOT } from "../config/config";
import axios from "axios";
import { addEntities } from "./entities.actions";
import { setTotal } from "./group.actions";
import { getItemQuery } from "../selectors";
import { logout } from "./auth.actions";
import getAuthHeader from "../helpers/getAuthHeaders";
import { changeToAvailableNeighborOrClose } from "./ui.actions";

export const addItem = createAction("ADD_ITEM");
export const addItems = createAction("ADD_ITEMS");
export const removeItem = createAction("REMOVE_ITEM");
export const removeItems = createAction("REMOVE_ITEMS");
export const updateItem = createAction("UPDATE_ITEM");

export const itemsRequest = createAction("ITEM_REQUEST");
export const itemsRequestSuccess = createAction("ITEM_REQUEST_SUCCESS");
export const itemsRequestFailure = createAction("ITEM_REQUEST_FAILURE");

export const getItems = () => {
  return async (dispatch, getState) => {
    const state = getState();
    dispatch(itemsRequest());
    try {
      const query = getItemQuery(state);
      const { data } = await axios.get(
        API_ROOT + "/items?" + query,
        getAuthHeader()
      );
      const { entities } = normalizeItems(data.items);
      console.log(data);
      console.log(entities);

      dispatch(
        itemsRequestSuccess({
          message: "items fetched",
          status: 200
        })
      );
      dispatch(addEntities(entities));
      dispatch(setTotal(data.count));
    } catch (err) {
      const status = err.response ? err.response.status : 400;
      dispatch(
        itemsRequestFailure({
          message: err.message,
          status: status
        })
      );
      if (status === 401) dispatch(logout());
    }
  };
};

export const putItemUpdate = options => {
  return async dispatch => {
    dispatch(itemsRequest());
    try {
      const { data, status } = await axios.put(
        API_ROOT + "/items/" + options.id,
        options,
        getAuthHeader()
      );
      if (status === 401) dispatch(logout());

      dispatch(
        itemsRequestSuccess({
          message: "item fetched",
          status: 200
        })
      );
      const { entities } = normalizeItem(data);
      dispatch(addEntities(entities));
    } catch (err) {
      const status = err.response ? err.response.status : 400;
      dispatch(
        itemsRequestFailure({
          message: err.message,
          status: status
        })
      );
      if (status === 401) dispatch(logout());
    }
  };
};

export const putBulkItemUpdate = options => {
  return async dispatch => {
    dispatch(itemsRequest());
    try {
      const { data, status } = await axios.put(
        API_ROOT + "/items/",
        options,
        getAuthHeader()
      );
      if (status === 401) dispatch(logout());

      dispatch(
        itemsRequestSuccess({
          message: "item fetched",
          status: 200
        })
      );
      const { entities } = normalizeItems(data);
      dispatch(addEntities(entities));
    } catch (err) {
      const status = err.response ? err.response.status : 400;
      dispatch(
        itemsRequestFailure({
          message: err.message,
          status: status
        })
      );
      if (status === 401) dispatch(logout());
    }
  };
};

export const deleteItem = id => {
  return async dispatch => {
    dispatch(itemsRequest());
    try {
      const { data, status } = await axios.delete(
        API_ROOT + "/items/" + id,
        getAuthHeader()
      );
      if (status === 401) dispatch(logout());

      dispatch(
        itemsRequestSuccess({
          message: "item deleted",
          status: 200
        })
      );
      // const { entities } = normalizeItem(data);
      dispatch(changeToAvailableNeighborOrClose());
      dispatch(removeItem(id));
    } catch (err) {
      const status = err.response ? err.response.status : 400;
      dispatch(
        itemsRequestFailure({
          message: err.message,
          status: status
        })
      );
      if (status === 401) dispatch(logout());
    }
  };
};

export const deleteItems = (ids = []) => {
  return async dispatch => {
    dispatch(itemsRequest());
    try {
      const { data, status } = await axios.delete(API_ROOT + "/items/", {
        data: { ids },
        headers: {
          ...getAuthHeader().headers
        }
      });
      if (status === 401) dispatch(logout());

      dispatch(
        itemsRequestSuccess({
          message: data.message,
          status: 200
        })
      );
      dispatch(removeItems(data.deletedItems));
    } catch (err) {
      const status = err.response ? err.response.status : 400;
      dispatch(
        itemsRequestFailure({
          message: err.message,
          status: status
        })
      );
      if (status === 401) dispatch(logout());
    }
  };
};
