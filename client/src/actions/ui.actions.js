import { createAction } from "redux-starter-kit";
import { getLightBoxItemIds } from "../selectors";

export const openLightBox = createAction("OPEN_LIGHTBOX");
export const closeLightBox = createAction("CLOSE_LIGHTBOX");
export const changeItem = createAction("LIGHTBOX_ITEM_CHANGE");

export const toggleSelectionMode = createAction("TOGGLE_SELECTION_MODE");
export const selectItem = createAction("SELECT_ITEM");
export const selectItems = createAction("SELECT_ITEMS");
export const clearSelectedItems = createAction("SELECT_ITEMS_CLEAR");

export const changeScreen = createAction("SCREEN_CHANGE");

export const changeToAvailableNeighborOrClose = () => {
  return (dispatch, getState) => {
    const state = getState();
    const { prev, next } = getLightBoxItemIds(state);
    if (next) {
      dispatch(changeItem(next));
    } else if (prev) {
      dispatch(changeItem(prev));
    } else {
      dispatch(closeLightBox());
    }
  };
};
