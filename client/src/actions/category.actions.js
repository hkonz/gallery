import { createAction } from "redux-starter-kit";
import { normalizeCategories, normalizeCategory } from "../helpers/normalize";
import { API_ROOT } from "../config/config";
import axios from "axios";
import getAuthHeader from "../helpers/getAuthHeaders";

export const addCategory = createAction("ADD_CATEGORY");
export const addCategories = createAction("ADD_CATEGORIES");
export const removeCategory = createAction("REMOVE_CATEGORY");
export const removeCategories = createAction("REMOVE_CATEGORIES");
export const updateCategory = createAction("UPDATE_CATEGORY");

export const categoriesRequest = createAction("CATEGORY_REQUEST");
export const categoriesRequestSuccess = createAction(
  "CATEGORY_REQUEST_SUCCESS"
);
export const categoriesRequestFailure = createAction(
  "CATEGORY_REQUEST_FAILURE"
);

export const getCategoryList = () => {
  return async dispatch => {
    dispatch(categoriesRequest());
    try {
      const { data } = await axios.get(
        API_ROOT + "/tags/categories",
        getAuthHeader()
      );
      const { entities } = normalizeCategories(data);
      dispatch(addCategories(entities.categories));
      dispatch(categoriesRequestSuccess());
    } catch (err) {
      dispatch(categoriesRequestFailure());
    }
  };
};

export const createCategory = options => {
  return async dispatch => {
    dispatch(categoriesRequest());
    try {
      const { data } = await axios.post(
        API_ROOT + "/tags/categories",
        options,
        getAuthHeader()
      );
      const { entities } = normalizeCategory(data);
      dispatch(addCategories(entities.categories));
      dispatch(categoriesRequestSuccess());
    } catch (err) {
      dispatch(categoriesRequestFailure());
    }
  };
};
