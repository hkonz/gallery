import { createAction } from "redux-starter-kit";
import { normalizeFiles } from "../helpers/normalize";
import { API_ROOT } from "../config/config";
import axios from "axios";

export const addFile = createAction("ADD_FILE");
export const addFiles = createAction("ADD_FILES");
export const removeFile = createAction("REMOVE_FILE");
export const removeFiles = createAction("REMOVE_FILES");
export const updateFile = createAction("UPDATE_FILE");

export const getFiles = () => {
  return async dispatch => {
    const { data } = await axios.get(API_ROOT + "/files");

    const { entities } = normalizeFiles(data);

    console.log(entities);
    dispatch(addFiles(entities.files));
  };
};

export const getFile = id => {
  return async dispatch => {
    const { data } = await axios.get(API_ROOT + "/files/" + id);

    const { entities } = normalizeFiles(data);

    console.log(entities);
    dispatch(addFile(entities.files));
  };
};
