import { createAction } from "redux-starter-kit";
import axios from "axios";
import { API_ROOT } from "../config/config";

export const authRequest = createAction("AUTH_REQUEST");
export const authFailure = createAction("AUTH_FAILURE");
export const authSuccess = createAction("AUTH_SUCCESS");

export const loggedOut = createAction("LOGGED_OUT");

export const logout = () => {
  localStorage.removeItem("user");
  localStorage.removeItem("token");
  return dispatch => dispatch(loggedOut());
};

export const login = creds => {
  return async dispatch => {
    dispatch(authRequest());
    try {
      const { data } = await axios.post(API_ROOT + "/users/login", creds);
      console.log("data", data);

      localStorage.setItem("user", JSON.stringify(data.user));
      localStorage.setItem("token", data.token);
      dispatch(authSuccess(data));
    } catch (err) {
      dispatch(authFailure(err.message));
      localStorage.removeItem("user");
      localStorage.removeItem("token");
    }
  };
};
