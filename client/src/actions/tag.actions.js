import { createAction } from "redux-starter-kit";
import { normalizeTags, normalizeTag } from "../helpers/normalize";
import { API_ROOT } from "../config/config";
import axios from "axios";
import getAuthHeader from "../helpers/getAuthHeaders";

export const addTag = createAction("ADD_TAG");
export const addTags = createAction("ADD_TAGS");
export const removeTag = createAction("REMOVE_TAG");
export const removeTags = createAction("REMOVE_TAGS");
export const updateTag = createAction("UPDATE_TAG");

export const tagsRequest = createAction("TAGS_REQUEST");
export const tagsRequestSuccess = createAction("TAGS_REQUEST_SUCCESS");
export const tagsRequestFailure = createAction("TAGS_REQUEST_FAILURE");

export const getTagList = () => {
  return async dispatch => {
    dispatch(tagsRequest());
    try {
      const { data } = await axios.get(API_ROOT + "/tags", getAuthHeader());
      const { entities } = normalizeTags(data);
      dispatch(addTags(entities.tags));
      dispatch(tagsRequestSuccess());
    } catch (err) {
      dispatch(tagsRequestFailure());
    }
  };
};

export const createTag = options => {
  return async dispatch => {
    dispatch(tagsRequest());
    try {
      const { data } = await axios.post(
        API_ROOT + "/tags",
        options,
        getAuthHeader()
      );
      const { entities } = normalizeTag(data);
      console.log("entities", entities);
      dispatch(addTags(entities.tags));
      dispatch(tagsRequestSuccess());
    } catch (err) {
      dispatch(tagsRequestFailure());
    }
  };
};
