const express = require("express");
const router = express.Router();
const db = require("../db/database");
const auth = require("../helpers/authenticate");

/* GET files listing. */
router.get("/", async function(req, res, next) {
  const files = await db.File.findAll();
  res.send(files);
});

router.get("/:id", async function(req, res, next) {
  const _id = req.params.id;
  const file = await db.File.findByPk(_id);
  res.set("Content-Type", file.mimetype);
  res.send(file.buffer);
});

module.exports = router;
