const express = require("express");
const router = express.Router();
const jwt = require("jsonwebtoken");
const auth = require("../helpers/authenticate");

const db = require("../db/database");
const { User, Role } = db;

/* GET users listing. */
router.get("/", auth(["ROLE_ONE", "ROLE_KATEGORIE_A"]), async function(
  req,
  res,
  next
) {
  const users = await User.findAll();
  res.send(users);
});

/* CREATE user. */
router.post("/create", async function(req, res, next) {
  const { username, password } = req.body;
  const user = await User.create({ username, password });
  res.send(user);
});

/* LOGIN user. */
router.post("/login", async function(req, res, next) {
  try {
    const { username, password } = req.body;
    let user = await User.findOne({
      where: { username },
      include: [
        {
          model: Role,
          as: "roles",
          attributes: ["name"]
        }
      ]
    });
    if (!user) {
      new Error("User/Password incorrect");
    }
    const match = await user.verifyPassword(password);

    if (!match) {
      new Error("User/Password incorrect");
    }

    user = user.get({
      plain: true
    });

    delete user.password;
    user.roles = user.roles.map(role => role.name);

    const token = await jwt.sign(
      { id: user.id, roles: user.roles },
      process.env.JWT_SECRET,
      {
        expiresIn: "1h"
      }
    );

    res.status(200).send({
      user,
      token,
      message: "Erfolgreich eingeloggt"
    });
  } catch (e) {
    res.status(400).send({ error: e.message });
  }
});

module.exports = router;
