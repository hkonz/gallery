const express = require("express");
const router = express.Router();

const itemService = require("../services/item.service");
const tagService = require("../services/tag.service");

/* GET home page. */
router.get("/", async function(req, res, next) {
  const categories = await itemService.findAll(req);
  const tags = await tagService.findTagsByItems();

  res.render("index", { categories, tags, title: "Gallery K41" });
});

// GET Homepage with group param
router.get("/group/:group", async function(req, res, next) {
  const categories = await itemService.findAll(req);
  const tags = await tagService.findTagsByItems();

  res.render("index", { categories, tags, title: "Gallery K41" });
});

module.exports = router;
