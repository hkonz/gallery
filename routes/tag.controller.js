const express = require("express");
const router = express.Router();
const db = require("../db/database");
const auth = require("../helpers/authenticate");
const roles = require("../client/src/constants/roles");
const tagService = require("../services/tag.service");

/* GET Tag listing. */
router.get("/", auth(), async function(req, res, next) {
  try {
    const instances = await tagService.findTags(req);
    res.json(instances);
  } catch (error) {
    res.status(400).send({ error: error.message });
  }
});

/* GET Category listing. */
router.get("/categories", auth(), async function(req, res, next) {
  try {
    const instances = await tagService.findCategories(req);
    res.json(instances);
  } catch (error) {
    res.status(400).send({ error: error.message });
  }
});

//
// /* GET single instance. */
// router.get("/:id", async function(req, res, next) {
//   const _id = req.params.id;
//   const instance = await tagService.findOne(_id);
//   res.send(instance);
// });

/* Create Tag . */
router.post("/", auth(roles.TAG_CREATE), async function(req, res) {
  try {
    const context = {
      ...req.body
    };
    const instance = await tagService.createTag(context);

    res.json(instance);
  } catch (error) {
    res.status(400).send({ error: error.message });
  }
});

/* Create Category . */
router.post("/categories", auth(roles.CATEGORY_CREATE), async function(
  req,
  res
) {
  try {
    const context = {
      ...req.body
    };
    const instance = await tagService.createCategory(context);

    res.json(instance);
  } catch (error) {
    res.status(400).send({ error: error.message });
  }
});

module.exports = router;
