const express = require("express");
const multer = require("multer");
const router = express.Router();
const db = require("../db/database");
const auth = require("../helpers/authenticate");
const roles = require("../client/src/constants/roles");
const tagService = require("../services/tag.service");

const { createItemFromFile } = require("../helpers/createItemFromFile");

// /* GET upload form. */
// router.get("/", async function(req, res, next) {
//   let categories = await db.Tag.findAll({ where: { isCategory: true } });
//   let tags = await db.Tag.findAll({ where: { isCategory: false } });
//   categories = categories.map(it => it.get({ plain: true }));
//   tags = tags.map(it => it.get({ plain: true }));
//
//   res.render("upload", { title: "Gallery K41", categories, tags });
// });

//-----
// multer

const upload = multer({
  limits: {
    fileSize: 1000000 * 24 // 24 MB
  },
  fileFilter(req, file, cb) {
    const type = file.mimetype.split("/")[0];
    if (["image", "video"].includes(type)) {
      return cb(undefined, true);
    }
    return cb(new Error("Please upload an image or a video"));
  }
});

router.post(
  "/",
  auth(roles.UPLOAD),
  upload.single("file"),
  async (req, res) => {
    try {
      const item = await createItemFromFile(req);
      res.json(item);
    } catch (error) {
      res.status(400).send({ error: error.message });
    }
  }
);

module.exports = router;
