const express = require("express");
const router = express.Router();
const db = require("../db/database");
const auth = require("../helpers/authenticate");
const itemService = require("../services/item.service");
const roles = require("../client/src/constants/roles");

const { Item } = db;

/* GET listing. */
router.get("/", auth(), async function(req, res, next) {
  try {
    const instances = await itemService.findAll(req);
    res.json(instances);
  } catch (error) {
    res.status(400).send({ error: error.message });
  }
});

/* GET single instance. */
router.get("/:id", auth(), async function(req, res, next) {
  try {
    const instance = await itemService.findOne(req);
    res.json(instance);
  } catch (error) {
    res.status(400).send({ error: error.message });
  }
});

/* DELETE single instance. */
router.delete("/:id", auth(roles.ITEM_DELETE), async function(req, res, next) {
  try {
    const deletedId = await itemService.deleteOne(req);
    res.json({ message: `item successfully deleted: id ${deletedId}` });
  } catch (error) {
    res.status(400).send({ error: error.message });
  }
});

/* DELETE multiple instances. */
router.delete("/", auth(roles.ITEM_DELETE), async function(req, res, next) {
  const deletedInstances = await itemService.deleteAll(req);

  res.json({
    message: `successfully deleted: item(${deletedInstances})`,
    deletedItems: deletedInstances
  });
});

/* UPDATE single instance. */
router.put("/:id", auth(roles.ITEM_UPDATE), async function(req, res, next) {
  try {
    const updatedInstance = await itemService.updateOne(req);
    res.json(updatedInstance);
  } catch (error) {
    res.status(400).send({ error: error.message });
  }
});

/* UPDATE multiple instances. */
router.put("/", auth(roles.ITEM_UPDATE), async function(req, res, next) {
  try {
    const updatedInstances = await itemService.updateAll(req);
    res.json(updatedInstances);
  } catch (error) {
    res.status(400).send({ error: error.message });
  }
});

module.exports = router;
