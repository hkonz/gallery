const db = require("../db/database");
const { Item, File, Tag, TagRole, CategoryItem, TagItem } = db;
const sortUserRoles = require("../helpers/sortUserRoles");

// helper
const createRoleName = input_name => {
  return (
    "ROLE_" +
    input_name
      .replace(/[^a-zA-Z ]/g, "")
      .split(" ")
      .join("_")
      .toUpperCase()
  );
};

// code
const createCode = input_name => {
  return input_name
    .replace(/[^a-zA-Z ]/g, "")
    .split(" ")
    .join("-")
    .toLowerCase();
};

// api
const createCategory = async context => {
  const { name, sequence = 1 } = context;
  return await Tag.create(
    {
      name,
      sequence,
      code: createCode(name),
      isCategory: true,
      role: {
        name: createRoleName(name),
        type: "CATEGORY",
        description: `Role for Category: "${name}"`
      }
    },
    {
      include: [TagRole]
    }
  );
};

const createTag = async context => {
  const { name, sequence = 1 } = context;
  return await Tag.create(
    {
      name,
      sequence,
      code: createCode(name),
      isCategory: false,
      role: {
        name: createRoleName(name),
        type: "TAG",
        description: `Role for Tag: "${name}"`
      }
    },
    {
      include: [TagRole]
    }
  );
};

const getCategoryOptions = req => {
  const { isAdmin, tagRoles, categoryRoles } = sortUserRoles(req.userRoles);
  let result = {
    where: {
      isCategory: true
    },
    include: [TagItem]
  };

  if (!isAdmin) {
    result = {
      ...result,
      where: {
        ...result.where,
        roleId: categoryRoles
      }
    };
  }
  return result;
};

const findCategories = async req => {
  const options = getCategoryOptions(req);
  return await Tag.findAll(options);
};

const findCategoryById = async (id, req) => {
  let options = getCategoryOptions(req);
  options = {
    ...options,
    where: {
      ...options.where,
      id: id
    }
  };
  return await Tag.findOne(options);
};

const getTagOptions = req => {
  const { isAdmin, tagRoles, categoryRoles } = sortUserRoles(req.userRoles);
  let result = {
    where: {
      isCategory: false
    },
    include: [TagItem]
  };

  if (!isAdmin) {
    result = {
      ...result,
      where: {
        ...result.where,
        roleId: tagRoles
      }
    };
  }
  return result;
};

const findTags = async req => {
  const options = getTagOptions(req);
  return await Tag.findAll(options);
};

const findTagsByIds = async (arrayOfIds, req) => {
  let options = getTagOptions(req);
  options = {
    ...options,
    where: {
      ...options.where,
      id: arrayOfIds
    }
  };

  return await Tag.findAll(options);
};

const findTagsByItems = async params => {
  const tagRecords = await Tag.findAll({
    where: {
      isCategory: false
    },
    include: [TagItem]
  });

  return tagRecords;
};

module.exports = {
  createTag,
  createCategory,
  findCategories,
  findTags,
  findTagsByItems,
  findTagsByIds,
  findCategoryById
};
