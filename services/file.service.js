const sharp = require("sharp");

const thumbSize = {
  width: 190,
  height: 190
};

const processImageBuffer = async (inputBuffer, resizeOptions) => {
  let buffer;
  if (!resizeOptions) {
    buffer = inputBuffer;
  } else {
    buffer = await sharp(inputBuffer)
      .rotate()
      .resize(resizeOptions)
      .jpeg()
      .toBuffer();
  }
  const meta = await sharp(buffer).metadata();

  return {
    buffer,
    mimetype: "image/" + meta.format,
    size: meta.size,
    width: meta.width,
    height: meta.height
  };
};

const getResizeOptionsForMedium = fileOptions => {
  const { width, height } = fileOptions;
  const max_a = 1024;
  const max_b = 1920;
  const MEDIUM_WIDTH = 1024;

  const isBigger =
    (width > max_a && height > max_b) || (width > max_b && height > max_a);

  const longSide = width >= height ? "width" : "height";

  if (isBigger) {
    return {
      [longSide]: MEDIUM_WIDTH
    };
  }
  return null;
};

const getSizeFromImage = async input => {
  const meta = await sharp(input).metadata();
  return {
    width: meta.width,
    height: meta.height
  };
};

const getResizeOptions = options => {
  const { gridWidth, gridHeight } = options;
  const { width, height } = thumbSize;

  return {
    width: gridWidth * width,
    height: gridHeight * height
  };
};

module.exports = {
  getResizeOptions,
  getSizeFromImage,
  getResizeOptionsForMedium,
  processImageBuffer
};
