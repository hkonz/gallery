const db = require("../db/database");
const { Item, File, Tag } = db;
const { Op } = db.Sequelize;
const groupBy = require("../helpers/groupBy");
const sortUserRoles = require("../helpers/sortUserRoles");
const tagService = require("./tag.service");

const parseSorting = sortString => {
  // eventDate+desc,tags.name+asc

  let result = [];
  const allCriteria = sortString.split(",");
  allCriteria.forEach(it => {
    const [propName, direction] = it.split(" ");
    const criteria = [...propName.split("."), direction];
    result.push(criteria);
  });
  return result;
};

const getOptions = req => {
  const { userRoles } = req;
  const { limit = 1, page = 1, sort = "" } = req.query;
  const { isAdmin, tagRoles, categoryRoles } = sortUserRoles(userRoles);

  let include_File = {
    model: File,
    as: "files",
    attributes: { exclude: ["buffer"] }
  };

  let include_Category = {
    model: Tag,
    as: "category"
  };

  let include_Tag = {
    model: Tag,
    as: "tags"
  };

  let result = {
    include: [include_File, include_Category, include_Tag],
    limit: parseInt(limit),
    offset: parseInt(limit) * (parseInt(page) - 1),
    order: parseSorting(sort),
    distinct: true
  };

  if (!isAdmin) {
    // category constraint
    include_Category.where = {
      roleId: {
        [Op.in]: categoryRoles
      }
    };

    // tags constraint
    include_Tag.required = false;
    include_Tag.where = {
      [Op.or]: {
        id: {
          [Op.is]: null
        },
        roleId: {
          [Op.in]: tagRoles
        }
      }
    };
  }
  return result;
};

const findAll = async req => {
  const options = getOptions(req);

  const { rows, count } = await Item.findAndCountAll(options);

  return {
    count,
    items: rows
  };
};

const findOne = async req => {
  const id = req.params.id;
  const { include } = getOptions(req);
  let instance = await Item.findByPk(id, { include });
  if (!instance) {
    throw new Error("No Item found width id: " + id);
  }
  return instance;
};

const deleteAll = async req => {
  const { ids = [] } = req.body;
  const { include } = getOptions(req);

  // find all item ids user is authorized to delete
  let instances = await Item.findAll({
    include,
    where: {
      id: ids
    },
    attributes: { include: ["id"] }
  });
  const idsToDelete = instances.map(
    it =>
      it.get({
        plain: true
      }).id
  );
  await Item.destroy({ where: { id: idsToDelete } });
  return idsToDelete;
};

const deleteOne = async req => {
  const id = req.params.id;
  const { include } = getOptions(req);
  let instance = await Item.findByPk(id, { include });
  if (!instance) {
    throw new Error("No Item found width id: " + id);
  }
  await instance.destroy();
  return id;
};

const updateAll = async req => {
  const result = [];
  const arrayOfUpdates = req.body;
  const { include } = getOptions(req);
  await Promise.all(
    arrayOfUpdates.map(async options => {
      let { id, tags, categoryId, ...propsToUpdate } = options;

      const instance = await Item.findByPk(id, { include });
      if (instance) {
        if (tags && tags.length) {
          const tagRecords = await tagService.findTagsByIds(tags, req);
          await instance.setTags(tagRecords);
        }
        if (categoryId) {
          const categoryRecord = await tagService.findCategoryById(
            categoryId,
            req
          );
          if (categoryRecord) {
            propsToUpdate = {
              ...propsToUpdate,
              categoryId
            };
          }
        }
        await instance.update(propsToUpdate);
        const updated = await Item.findByPk(id, { include });
        result.push(updated);
      }
    })
  );

  return result;
};

const updateOne = async req => {
  const props = {
    ...req.body,
    id: req.params.id
  };
  let { id, tags, categoryId, ...propsToUpdate } = props;
  const { include } = getOptions(req);

  const instance = await Item.findByPk(id, { include });
  if (!instance) {
    throw new Error("No Item found width id: " + id);
  }

  if (tags && tags.length) {
    const tagRecords = await tagService.findTagsByIds(tags, req);
    await instance.setTags(tagRecords);
  }

  if (categoryId) {
    const categoryRecord = await tagService.findCategoryById(categoryId, req);
    if (categoryRecord) {
      propsToUpdate = {
        ...propsToUpdate,
        categoryId
      };
    }
  }

  await instance.update(propsToUpdate);
  return await Item.findByPk(id, { include });
};

module.exports = {
  findAll,
  findOne,
  deleteAll,
  deleteOne,
  updateAll,
  updateOne
};
