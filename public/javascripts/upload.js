const categorySelect = document.getElementById("category");
const eventDate = document.getElementById("event-date");
const uploadBtn = document.getElementById("upload-btn");
const $tagSelect = $("#tags");
const $fileInput = $("#file-upload");

$fileInput.on("change", function() {
  var $fileList = $("#file-list");
  console.log("this.files", this.files);

  var $ul = $('<ul class="list-group list-group-flush"></ul>');

  for (var key in this.files) {
    if (this.files.hasOwnProperty(key)) {
      var file = this.files[key];
      var li =
        '<li class="list-group-item" data-filename="' +
        file.name +
        '">' +
        '<div class="d-flex justify-content-between">' +
        "<span>" +
        file.name +
        "</span>" +
        "<span>" +
        humanFileSize(file.size, true) +
        '<span> <i class="material-icons text-white">' +
        "check" +
        "</i></span>" +
        "</span>" +
        "</div>" +
        '<div class="progress">' +
        '  <div class="progress-bar" role="progressbar" style="width: 0%" aria-valuemin="0" aria-valuemax="100"></div>\n' +
        "</div>" +
        "</li>";
      $ul.append(li);
    }
  }

  $fileList.html($ul);
});

uploadBtn.addEventListener("click", function() {
  const fileInput = document.getElementById("file-upload");
  console.log(fileInput.files);
  for (let key in fileInput.files) {
    if (fileInput.files.hasOwnProperty(key)) {
      const file = fileInput.files[key];

      console.log("", file);
      sendRequest(file);
    }
  }
});

function sendRequest(file) {
  return new Promise((resolve, reject) => {
    const req = new XMLHttpRequest();

    req.upload.addEventListener("progress", event => {
      if (event.lengthComputable) {
        var state = (event.loaded / event.total) * 100;
        console.log("progress", event, state);

        setProgressBar(file.name, state);
        // const copy = { ...this.state.uploadProgress };
        // copy[file.name] = {
        //   state: "pending",
        //   percentage: (event.loaded / event.total) * 100
        // };
        // this.setState({ uploadProgress: copy });
      }
    });

    req.upload.addEventListener("load", event => {
      console.log("load", file.name, event);

      setProgressBar(file.name, 100);
      // const copy = { ...this.state.uploadProgress };
      // copy[file.name] = { state: "done", percentage: 100 };
      // this.setState({ uploadProgress: copy });
      resolve(req.response);
    });

    req.upload.addEventListener("error", event => {
      console.log("error", file.name, event);

      // const copy = { ...this.state.uploadProgress };
      // copy[file.name] = { state: "error", percentage: 0 };
      // this.setState({ uploadProgress: copy });

      reject(req.response);
    });

    const formData = new FormData();
    formData.append("file", file, file.name);
    formData.append("category", categorySelect.value);
    formData.append("id", uuidv4());
    formData.append("tags", $tagSelect.val());
    if (eventDate.value) {
      formData.append(
        "eventDate",
        getDateFromGermanDateString(eventDate.value)
      );
    }

    console.log("categorySelect.value", categorySelect.value);
    console.log("formData", formData);

    req.open("POST", "/upload");
    req.send(formData);
  });
}

function humanFileSize(bytes, si) {
  var thresh = si ? 1000 : 1024;
  if (Math.abs(bytes) < thresh) {
    return bytes + " B";
  }
  var units = si
    ? ["kB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB"]
    : ["KiB", "MiB", "GiB", "TiB", "PiB", "EiB", "ZiB", "YiB"];
  var u = -1;
  do {
    bytes /= thresh;
    ++u;
  } while (Math.abs(bytes) >= thresh && u < units.length - 1);
  return bytes.toFixed(1) + " " + units[u];
}

function getDateFromGermanDateString(germanDateString) {
  var dateString = germanDateString
    .split(".")
    .reverse()
    .join("-");
  return new Date(dateString);
}

function setProgressBar(filename, state) {
  var $wrapper = $('[data-filename="' + filename + '"]');
  var $progressBar = $wrapper.find(".progress-bar");

  $progressBar.css("width", state + "%");

  if (state === 100) {
    $wrapper
      .find(".material-icons")
      .removeClass("text-white")
      .addClass("text-success");
  }
}

function uuidv4() {
  return ([1e7] + -1e3 + -4e3 + -8e3 + -1e11).replace(/[018]/g, c =>
    (
      c ^
      (crypto.getRandomValues(new Uint8Array(1))[0] & (15 >> (c / 4)))
    ).toString(16)
  );
}
