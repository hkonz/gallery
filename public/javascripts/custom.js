// -------
// Fancy Box

const $fancyBox = $().fancybox({
  selector: '[data-fancybox="gallery"]:visible',
  // Options will go here
  animationEffect: "zoom",

  // Duration in ms for open/close animation
  animationDuration: 250,

  // Should image change opacity while zooming
  // If opacity is "auto", then opacity will be changed if image and thumbnail have different aspect ratios
  zoomOpacity: "auto",
  buttons: ["download", "fullScreen", "close"]
});

//---------
// Isotope

const $grid = $(".grid").isotope({
  // options
  itemSelector: ".grid-item",
  percentPosition: true,
  masonry: {
    // use element for option
    columnWidth: ".grid-sizer"
  },
  getSortData: {
    time: "[data-time]"
  },
  sortAscending: {}
});

$("button.sort-by").on("click", function() {
  const sortBy = $(this).data("sort-by");
  $grid.isotope({ sortBy });
});

$("button.filter").on("click", function() {
  const filter = $(this).data("filter");
  $grid.isotope({ filter });
  controlVisibilityOfGridTitles();
  $grid.isotope("layout");
  // set filter in hash
  location.hash = "filter=" + encodeURIComponent(filter);
});

var $filterExclusive = $(".filter-exclusive");
var $filterInclusive = $(".filter-inclusive");

$filterExclusive.add($filterInclusive).on("change", function() {
  var exclusives = [];
  var inclusives = [];
  var filter = "";

  $filterExclusive.each(function(i, elem) {
    if (elem.checked) {
      exclusives.push($(elem).data("filter"));
    }
  });

  $filterInclusive.each(function(i, elem) {
    if (elem.checked) {
      inclusives.push($(elem).data("filter"));
    }
  });

  exclusives = exclusives.join("");

  if (inclusives.length) {
    // map inclusives with exclusives for
    filter = $.map(inclusives, function(value) {
      return value + exclusives;
    });
    filter = filter.join(", ");
  } else {
    filter = exclusives;
  }

  // if (inclusives.length) {
  //   filter = inclusives.join(", ");
  // }
  $grid.isotope({ filter });
  controlVisibilityOfGridTitles();
  $grid.isotope("layout");
  // set filter in hash
  location.hash = "filter=" + encodeURIComponent(filter);
});

$grid.on("arrangeComplete", function(event, filteredItems) {
  var $gridTitle = $(event.currentTarget)
    .parents(".grid-wrapper")
    .find(".grid-title");

  if (!filteredItems.length) {
    $gridTitle.hide();
  } else {
    $gridTitle.show();
  }
});

$(function() {
  $grid.isotope("layout");

  var isIsotopeInit = false;

  function onHashchange() {
    var hashFilter = getHashFilter();
    if (!hashFilter && isIsotopeInit) {
      return;
    }
    isIsotopeInit = true;

    if (hashFilter) {
      // check checkboxes
      var splitHashFilter = hashFilter.split(", ");
      splitHashFilter.forEach(function(filter) {
        $('.filter-checkbox[data-filter="' + filter + '"]').prop(
          "checked",
          true
        );
      });

      // filter isotope
      $grid.isotope({
        itemSelector: ".element-item",
        filter: hashFilter
      });
    }
  }

  $(window).on("hashchange", onHashchange);
  // trigger event handler to init Isotope
  onHashchange();
});

//--------------
// helpers

function getHashFilter() {
  var hash = location.hash;
  // get filter=filterName
  var matches = location.hash.match(/filter=([^&]+)/i);
  var hashFilter = matches && matches[1];
  return hashFilter && decodeURIComponent(hashFilter);
}

function controlVisibilityOfGridTitles() {
  var $gridWrapper = $(".grid-wrapper");
  $gridWrapper.each(function() {
    var $visibleItems = $(this).find(".grid-item:visible");
    var $gridTitle = $(this).find(".grid-title");
    if ($visibleItems.length) {
      $gridTitle.fadeIn();
    } else {
      $gridTitle.fadeOut();
    }
  });
}
